% Class:      ASEN 6519 - System Identification
% 
% File:       SysId_Model
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Longitudinal Aircraft Model simulation

close all, clear all, clc

%% Preamble
% rSec defines which sections to run
% [0-3 (model & reference definition), 4-10 (control), 11-17 (sys ID)] 
% rSec = [1, 0, 1] % System Identification option
rSec = [1, 1, 0] % Control Option


% Choose which demonstration to look at, control or system identification
% Note that they use slightly different systems from eachother which is 
% why this distinction is necessary
if rSec(2) == 1
    whichDem = 'control'
else
    whichDem = 'SysID'
end

% If o_data_flag = 0 recalculate pseudo random excitation and fft data else
% use matFile2use data
o_data_flag = 0;
matFile2use = 'final_best_data_4_14_17.mat';

%% 0. State Space Model
disp('-------------- 0. Model Definition --------------')

% State vector x = [u w q theta x z] 
% where: 
% u = perturbed inertial X velocity component
% w = perturbed inertial Z velocity component
% q = perturbed Y rotational rate
% theta = perturbed pitch angle
% x = position in inertial X direction
% z = position in inertial Z direction
 
A = [-.3206 .3514   0       -9.8048 0   0;
     -.9338 -6.8632 19.8386 -.3187  0   -.0004;
     0      -2.5316 -3.9277 0       0   0;
     0      0       1       0       0   0;
     1      0       0       0       -.1 0;
     0      1       0       -21     0   0];
 
B = [.0037  11.2479;
     -.1567 0;
     -.9332 0;
     0      0;
     0      0;
     0      0]; 
 
if strcmp('SysID', whichDem) == true
    B = B(:,1);
end

% Input vector u = [elevator; throttle];
C = [0 0 0 0 1 0;
     0 0 0 0 0 1];
C_s = [0 0 0 0 0 1];

out1 = 'X pose'
out2 = 'Z pose'

D = [0];

states = {'du' 'dw' 'dq' 'theta' 'X' 'Z'};
inputs = {'Elevator'};
outputs = {'X Pose' 'Z Pose'}

sys_ol = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs); 

nStates = length(C)
nOut = size(C,1)
nIn = size(B,2)

gain = 1
%% Servo Model
% elev_deflection_limit = 45; % [deg]
% elev_deflection_band = 45/sqrt(2) % [deg]
% 
% sys_servo = servomodel();
% As = sys_servo.A;
% Bs = sys_servo.B;
% Cs = sys_servo.C;
% Ds = sys_servo.D;

%% 1. Reference definition
disp('-------------- 1. Reference Definition --------------')

% Generate step response reference command for overshoot/settling time
% requirements
waypoints = [1, 2, 3];
time = 100;
dt = .01;
t = 0:dt:time;
time_sin = 20;
tsine = 0:dt:time_sin;
time_step = 50;
tstep = 0:dt:time_step;
t1 = 10/dt;
t2 = 35/dt;
t3 = 60/dt;
r = t;
r(1:t1) = 0;
r(t1:t2) = waypoints(1);
r(t2:t3) = waypoints(2);
r(t3:end) = waypoints(3);

% Generate sinusoidal reference for tracking 
freq = 1;  % frequency [rad/s]
amp = 1;   % amplitude [m]
rsin = amp*sin(freq*tsine);
if nIn == 2
    rsin = [zeros(length(tsine),1), rsin(:)];
end

% Generate step reference
rstep = ones(1,numel(tstep));
if nIn == 2
    rstep = [zeros(length(tstep),1), rstep(:)];
end

poseSlope = 22;

% r =[poseSlope*t' ,r(:)]; %step on Z perturbation
if nIn == 1
    r =[r(:)]; %step on Z perturbation
else
    r =[zeros(length(t),1) ,r(:)]; %step on Z perturbation
end
    
% Each waypoint is a 1 meter step. Set bount to be .2 (20%) of step
b.bound = t;
b.bound(:) = .2;  % 20% 
ts = 10;
% Bound idices
b.b1 = t1+ts/dt;
b.b2 = t2+ts/dt;
b.b3 = t3+ts/dt;
% figure
% Call bounds.m script to plot reference and bounds
% bounds
% title('Reference and Constraints')
% xlabel('Time [s]')

%% 2. Modes, Eigenspaces, Lyapunov/Asymptotical Stability
disp('-------------- 2. Modes, Eigenspaces, Lyapunov/Asymptotical Stability --------------')
% -- Eigen values -- 
[eigV, eigD] = eig(A);
poles_ol = diag(eigD)
% -- Eigencoordinate representation -- 
T = eigV;
LAMBDA = inv(T)*A*T;
GAMMA = inv(T)*B;
sys_eig = ss(LAMBDA,GAMMA,C,D);

% -- Simulate --
times = 35;
ts = 0:.1:times;

% Add Modal simulation here. Reference Ramya's paper
    
%% 3. Reachability & Controllability 
disp('-------------- 3. Reachability % Controllability --------------')

% -- Assess controlability/reachability -- 
rankCont = rank(ctrb(A,B))

% -- Assess the observability of this system -- 
rankObsv = rank(obsv(A,C))

%% BEGIN SYSTEM CONTROL SECTION
disp('-- BEGIN SYSTEM CONTROL SECTION --')

%% 4. State Feedback Controller
disp('-------------- 4. State Feedback Controller --------------')
% -- Determine Desired Poles -- 
perturbations = [-.3; 0; 0; -.3; -.3; -.1];
poles_cl = perturbations + poles_ol

% -- Use place command to generate K matrix -- 
K_f = gain*place(A,B,poles_cl)

% F = inv(C/(-A+B*K)*B)   % DC Tracking controller
% F1 = eye(2);              % We did this in a  homework
% F = (D-1)/(C/(A-B*K)*B)   % No steady state error
% F = -inv(C/(A-B*K)*B)
% Use least squares solution to H*F = eye(3) where H = C*(-A+B*K)^-1*B
% H = C/(-A+B*K)*B
% F = inv(H'*H)*H'*eye(3)

if nIn == 1
    F = inv(C_s/(-A+B*K_f)*B)
else
    F = inv(C/(-A+B*K_f)*B)
end

% -- Define CL matrices -- 
Acl = A - B*K_f;
Bcl = B*F;
sys_cl = ss(Acl,Bcl,C,D,'statename',states,'inputname',inputs,'outputname',outputs);
[p_cl, z_cl] = pzmap(sys_cl);
n_cl_z = size(z_cl)
n_cl_p = size(p_cl)


% -- Closed loop eigenvectors -- 
[V_cl, d_cl] = eig(Acl);
eig_cl = diag(d_cl)

% -- Simulate the system -- 
[yfcl,~,xfcl] = lsim(sys_cl,r,t);
[yfcl2,~,xfcl2] = lsim(sys_cl,rsin,tsine);
[yfcl3,~,xfcl3] = lsim(sys_cl,rstep,tstep);


% -- Calculate control effort -- 
u_f = -K_f*xfcl' + F*r';
u_f2 = -K_f*xfcl2' + F*rsin';
u_f3 = -K_f*xfcl3' + F*rstep';

% -- Plot Simulation Results -- 
label_p = 'Feedback';
hold on, grid on
simplot(t, r, yfcl, xfcl, t, u_f, label_p, 0, 0, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yfcl, r, 2, label_p);

simplot2(tsine, rsin, yfcl2, xfcl2, tsine, u_f2, label_p, 0, 0, nStates, nOut)

simplot2(tstep, rstep, yfcl3, xfcl3, tstep, u_f3, label_p, 0, 0, nStates, nOut)

if rSec(2) ~= 0
%% 5. Integral control
disp('---------------- 5. Integral Control --------------')
Aa = [A zeros(nStates,nIn);
      -C zeros(nOut,nIn)];
Ba = [B; zeros(nIn,nIn)];
Fa = [zeros(size(B)); eye(nIn)];
Ca = [C zeros(nOut,nIn)];
Da = 0;

if nIn == 1
    Aa = [A zeros(nStates,nOut);
          -C zeros(nOut,nOut)];
    Ba = [B; zeros(nOut,nIn)];
    Fa = [zeros(size(B)); eye(nIn)];
    Ca = [C zeros(nOut,nOut)];
end
    
dPole_i = [poles_cl; -10.1; -9.5]

% -- Is the system controllable? -- 
rank = rank(ctrb(Aa,Ba))

% -- Calculate new K matrix -- 
Ka = place(Aa,Ba,dPole_i);
AaCL = Aa - Ba*Ka;
BaCL = Fa;
eigIcl = eig(AaCL);

% -- Form new SS system -- 
sysiCl = ss(AaCL, BaCL, Ca, Da);

% -- Simulate with first reference input -- 
[yicl,~,xicl] = lsim(sysiCl,r,t);

% -- Calculate actuator effort: -- 
u_i = -Ka*xicl';

% -- Plot Simulation Results --
label_p = 'Integral';
hold on, grid on
simplot(t, r, yicl, xicl, t, u_i, label_p, 0, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yicl, r, 2, label_p);

%% 6. Observer + Integral
disp('---------------- 6. Integral + Observer --------------')
% My system originally has 6 states. Add 2 for integral control and 6 more
% to reconstruct my states for the observer. 
% Observer states = 6 + 2 + 6 = 14

% Choose observer poles
% dobsvp = [-.05 -4 -1.1 -.13 -3 -2]; % design
% dobsvp = [-1 -16 -10 -1.6688+4.4506i -1.6688-4.4506i -5];
% dobsvp = [-1 -36 -10 -1.7 -1.6688 -55];
% dobsvp = [-.2 -.3 -40 -2 -3 -.1];   % slow observer, 2x more magnitude than open loop poles
% dobsvp = [-100 -6.69 -6.69 -2.7 -2.7 -.1];   % fast observer, 10x more magnitude than closed loop poles
% dobsvp = [-5 -4.5 -200 -10 -15 -3.1]   % slow observer, 10x more magnitude than open loop poles
t_factor = 10000;      % Tuning Factor
dpoles_o = t_factor.*poles_cl

% Use place command to get Luenberger observer gain via dual system:
L = (place(A',C',dpoles_o))';

Fo = [     zeros(size(B)); 
                eye(nIn);
      zeros(nStates,nIn)];                              % augment for observer error states
Ao = [Aa - Ba*Ka,                 Ba*Ka(:,1:nStates);   % Ignore K integral terms
      zeros(nStates,nStates+nIn), A-L*C];               % nIn here maybe should be replaced by number of integral additions?
Bo = Fo;
Co = [C zeros(nOut,nStates+nOut)];
Do = zeros(size(Co,1),size(Bo,2));

xoIC = zeros*ones(size(Ao,1),1);
% xoIC = [.05 .05 .05 .0175 .0175 .1 .1 .1 .1 .1 .1 .1 .1 .1]
% xoIC = [1 .05 .05 .0349 .0349 .01 .01 .01 0 -.01 .01 0 0 .01]
syso = ss(Ao,Bo,Co,Do);
[yio, ~, xio] = lsim(syso,r,t,xoIC);

% -- Calculate actuator effort --
u_io = -[Ka Ka(:,1:nStates)]*xio';

% -- Plot Simulation Results -- 
label_p = 'Observer + Integral';
hold on, grid on
simplot(t, r, yio, xio, t, u_io, label_p, 1, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yio, r, 2, label_p);


%% 7. LQR 
disp('---------------- 7. LQR --------------')

% Define alpha, 6 states
alpha = [1/1000 1 1/100000 1 1 10000]
% Scale alpha values
alpha = alpha./sum(alpha);
% Define Q state deviation penalization matrix
    % The maximum state error defined in the LQR section are as follows:
    % X vel, Z vel, Y rot rate, Pitch angle, X Pose, Z pose
    % 10^2   .5^2         .5^2      .349^2    .349^2   .01^2 
Q = diag(alpha./[10^2 .5^2 1^2 .349^2 10^2 .01^2]);

beta = 1;
rho = 10000;
% Actuator constraints
ucon = .5236;    % [rad] = 30 degrees
% Define R actuator penalization matrix
if nIn == 1
    R = rho*diag([1/ucon^2]);
elseif nIn == 2
    R = rho*diag([1/ucon^2 1/ucon^2]);
end
    
[K_lqr,~,~] = lqr(sys_ol,Q,R);

if nIn == 1
    F_lqr = inv(C_s/(-A+B*K_lqr)*B)
else
    F_lqr = inv(C/(-A+B*K_lqr)*B)
end

A_lqr = A - B*K_lqr;
eigcl = eig(A_lqr)
B_lqr = B*F_lqr;
% B_lqr = zeros(size(B))
sys_lqr = ss(A_lqr,B_lqr,C,D);
[y_lqr, ~, x_lqr] = lsim(sys_lqr,r,t);
u_lqr = -K_lqr*x_lqr' + F*r';

[y_lqr2, ~, x_lqr2] = lsim(sys_lqr,rsin,tsine);
u_lqr2 = -K_lqr*x_lqr2' + F*rsin';

% -- Plot Simulation Results -- 
label_p = 'LQR';
hold on, grid on
simplot(t, r, y_lqr, x_lqr, t, u_lqr, label_p, 0, 0, nStates, nOut, b)
e_settime = errorcalcs(b.b1, y_lqr, r, 2, label_p);

simplot2(tsine, rsin, y_lqr2, x_lqr2, tsine, u_lqr2, label_p, 0, 0, nStates, nOut)

%% 8. LQR + integral
disp('---------------- 8. LQR + Integral --------------')
% Before I can add the observer, design LQR + integral
% Define open loop augmented state matrices
Aaol = [A zeros(nStates,nOut); -C zeros(nOut,nOut)];
Baol = [B; zeros(nIn,nIn)];
Faol = [zeros(size(B)); eye(nIn)];
Caol = [C zeros(nOut)];
Daol = 0;
aolsys = ss(Aaol,Baol,Caol,Daol);

% Define alpha, 6 states + 2 error(integral)
alpha = [1/1000 1 1/100000 1 1 1/100 1/100 1]
% Scale alpha values
alpha = alpha./sum(alpha);
% Define Q state deviation penalization matrix
    % The maximum state error defined in the LQR section are as follows:
    % X vel, Z vel, Y rot rate, Pitch angle, X Pose, Z pose
    % 10^2   .5^2         .5^2      .349^2    .349^2   .01^2 
Qa = diag(alpha./[10^2 .5^2 1^2 .349^2 10^2 .01^2 .001^2 .001^2]);

beta = 1;
rho = 100000;
% Actuator constraints
ucon = .5236;    % [rad] = 30 degrees
% Define R actuator penalization matrix
Ra = rho*diag([1/ucon^2 .5/ucon^2]);

[Ka_lqr,Wa,clEvalsAug] = lqr(aolsys,Qa,Ra);

Facl = Faol;
Aacl = Aaol - Baol*Ka_lqr;
eigacl = eig(Aacl)
Bacl = Facl;
sys_lqr = ss(Aacl,Bacl,Caol,Daol);
[y_lqr_cl, ~, x_lqr_cl] = lsim(sys_lqr,r,t);
u_lqr = -Ka_lqr*x_lqr_cl';

% figure
% pzmap(sys_lqr)

% -- Plot Simulation Results -- 
label_p = 'LQR + Integral';
hold on, grid on
simplot(t, r, y_lqr_cl, x_lqr_cl, t, u_lqr, label_p, 0, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, y_lqr_cl, r, 2, label_p);

%% 9. LQR + Integral + Observer
disp('---------------- 9. LQR + Integral + Observer --------------')
% Define closed loop, integral, feeforward, LQR + observer state matrices
Faclo = [Faol; zeros(nStates,nOut)];
Aaclo = [Aaol - Baol*Ka,                  Baol*Ka(:,1:nStates); % Ignore Ki terms
             zeros(nStates,nStates+nOut), A-L*C];        
Baclo = Faclo;
Caclo = [Caol zeros(nOut,nStates)];
Daclo = zeros(size(Caclo,1),size(Baclo,2));

sys_lqri = ss(Aaclo,Baclo,Caclo,Daclo);
% xcloIC = .1*ones(14,1);

[y_lqri_cl, ~, x_lqri_cl] = lsim(sys_lqri,r,t,xoIC);

% Calculate actuator effort:
u_lqri = -[Ka_lqr Ka_lqr(:,1:nStates)]*x_lqri_cl';

% -- Plot Simulation Results -- 
label_p = 'LQR + Integral + Observer';
hold on, grid on
simplot(t, r, y_lqri_cl, x_lqri_cl, t, u_lqri, label_p, 1, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, y_lqri_cl, r, 2, label_p);

end % end control section block
%% 10. Plant Analysis
disp('---------------- 10. Plant Analysis --------------')
% figure
% bode(sys_ol), grid on

% if rsec(3) ~= 0

if rSec(3) ~= 0 % begin sys id block
%% BEGIN SYSTEM IDENTIFICATION SECTION
disp('-- BEGIN SYSTEM IDENTIFICATION SECTION --')
%% 11. Loop Gain Analysis
disp('---------------- 11. Loop Gain Analysis --------------')
% close all, clc

% Tuning/display flags
ny_plot = 3;
delta = .1;   % should be .5
delta0 = .5;
% delta1 = .1;
% gain = 1;

% Loop Gain
s = tf('s');
Lg = gain*K_f/(s*eye(size(A,1)) - A)*B;
Lg = minreal(Lg)

figure
pzmap(Lg), grid on
figure
margin(Lg), grid on
[lgp, lgz] = pzmap(Lg);
n_Lg_p = size(lgp)
n_Lg_z = size(lgz)

% -- Nyquist from bode --
circlim = 1;
% figure
% nyquist(Lg)
[re, im, wn] = nyquist(Lg);
[mag, phase] = bode(Lg, wn);
mag = squeeze(mag);
phase = squeeze(phase);
imb = mag.*tand(phase);
for i = 1:1:numel(mag)
   if mag(i) > circlim
       mag(i) = log(mag(i));
   end
   if -imb(i) > circlim
       imb(i) = -log(-imb(i));
   elseif imb(i) > circlim
       imb(i) = log(imb(i));
   end
end

% -- Nyquist from nyquist --
re = squeeze(re);
im = squeeze(im);
for i = 1:1:numel(re)
   if re(i) > circlim
       re_l(i) = log(re(i));
   end
   if -im(i) > circlim
       im_l(i) = -log(-im(i));
   elseif im(i) > circlim
       im_l(i) = log(im(i));
   end
end

% -- Plot Nyquist's -- 
x = (-1-delta):.01:(-1+delta);
y = sqrt(delta^2 - (x+1).^2);
x0 = (-1-delta0):.01:(-1+delta0);
y0 = sqrt(delta0^2 - (x0+1).^2);
if ny_plot < 1
    figure
    plot(mag, imb), hold on
    plot(mag, -imb), grid on
    plot(x0,y0,'k--', x0,-y0,'k--')
    plot(x,y,'k--', x,-y,'k--')
    title('Nyquist [logscale] (data from bode.m)')
    xlabel('Re [log(mag>1)]')
    ylabel('Im [log(im>1)]')
end

if ny_plot < 2
    figure
    plot(re_l, im_l), hold on
    plot(re_l, -im_l), grid on
    plot(x,y,'k--')
    plot(x,-y,'k--')
    title('Nyquist [logscale] (data from nyquist.m)')
    xlabel('Re [log(mag>1)]')
    ylabel('Im [log(im>1)]')
end

if ny_plot == 3
    figure
    subplot(2,1,1)
    plot(re, im), hold on, grid on
    plot(re, -im, 'b')
    plot(-1, 0, 'r+')
    title('Full Nyquist')
    axis([-50, 2300, -1500, 1500])
    subplot(2,1,2)
    h1 = plot(re, im); hold on, grid on
    h2 = plot(x0,y0,'r--');
    h3 = plot(x0,-y0,'r--');
    h4 = plot(x,y,'k--');
    h5 = plot(x,-y,'k--');
    h6 = plot(re, -im, 'b');
    h7 = plot(-1, 0, 'r+');
    axis([-1.5, 2.5, -4, 4])
    title('Zoomed in Nyquist')
    legend([h1, h2, h4],'Nyquist', '\delta0', '\deltaf')
end


% -- Get sigma and epsilon bounds --
[mag_b, phase_b, w_b] = bode(Lg);     % Rerun bode of loop gain
m.mag_b = squeeze(mag_b);
p.phase_b = squeeze(phase_b);
w.w_b = w_b;

gain_high = 1.1
gain_low = .41

Lg_high = gain_high*K_f/(s*eye(size(A,1)) - A)*B;
Lg_low = gain_low*K_f/(s*eye(size(A,1)) - A)*B;

[m.mag_b_high, p.phase_b_high, w.w_b_high] = bode(Lg_high);     % Rerun bode of loop gain
[m.mag_b_low, p.phase_b_low, w.w_b_low] = bode(Lg_low);     % Rerun bode of loop gain

m.mag_b_high = squeeze(m.mag_b_high);
p.phase_b_high = squeeze(p.phase_b_high);
m.mag_b_low = squeeze(m.mag_b_low);
p.phase_b_low = squeeze(p.phase_b_low);

% -- epsilon performance bounds --
% Use: |Lg| = Lg/(1+Lg) to figure out epsilon bounds
track50 = (10^(.01/20))/(1+(10^(.01/20))); % necessary magnitude to track at 50%
track100 = (10^(26/20))/(1+(10^(26/20))) % necessary magnitude to track at 100%
disp(['50% tracking magnitude = ',num2str(track50)])
disp(['100% tracking magnitude = ',num2str(track100)])
    
e.epsilon = 1./squeeze(mag_b);
e.epsilonx = [.00001 .01 1.1];
e.epsilony = [26 26 1];

% -- Sigma performance bounds --
lg0 = m.mag_b./cosd(p.phase_b);
sig.sigma = abs(1-lg0)-delta;
sig.sigma = 20*log10(sig.sigma);

sig.sigmau = 20*log10(m.mag_b) - abs(20*log10(m.mag_b) - sig.sigma);

sig.sigmap = abs(1-p.phase_b)-delta;
sig.sigmaup = abs(1+p.phase_b)+delta;

sig.sigmap = 20*log10(sig.sigmap);
sig.sigmaup = 20*log10(sig.sigmaup);

figure
subplot(2,1,1)
h1 = semilogx(w.w_b, 20*log10(m.mag_b)); hold on, grid on
h2 = semilogx(w.w_b_high, 20*log10(m.mag_b_high), 'c--');
h3 = semilogx(w.w_b_low, 20*log10(m.mag_b_low), 'c--');
h4 = semilogx(e.epsilonx, e.epsilony, 'k--');
h5 = semilogx(w.w_b, sig.sigma, 'r--');
h6 = semilogx(w.w_b, sig.sigmau, 'r--');
legend([h1, h2, h4, h5],'Bode', 'U/L Gain Bnd', '\epsilon Bnd', '\sigma Bnd')
title('Performance Bounds')
ylabel('[dB]')
axis([10^-2 10^1 -50 75])
subplot(2,1,2)
semilogx(w.w_b, p.phase_b), hold on, grid on
title('Performance Bounds')
axis([10^-2 10^(1) -125 0]);
ylabel('Phase [\circ]')
xlabel('\omega [rad/s]')

%% 12. ETFE (Empirical Transfer Function Estimate)
disp('---------------- 12. ETFE --------------')

decades = 3                 % Choose # of decades above and below center frequency to calculate
divisor = 10^decades;
ubound = divisor/100
wc = 1                      % [rad/s] my systems "critical" frequency
dw = wc/divisor             % [rad/s] delta omega
T = ((2*pi)/dw)             % [s] period. Rounded so that N is divisible by 2
dt = 2*pi/(2*(ubound)*wc)    % [s] dt nyquist at # decade above wc
N = T/dt                    % [] 

% --  Psuedo Random Excitation --
nfunc = round(N/2-1)        % Number of cosine functions 
phases = 2*pi*rand(1,nfunc);% Random phases
% ts = 10;                  % Settling time
mx = 10;                    %
time = 0:dt:mx*T;           % Total simulation time 
% freqs = (0:1:nfunc)*dw;   % Generate range of frequencies
freqs = (dw:1:nfunc+1)*dw;  % Generate range of frequencies
dc_flag = 0;

amp = ones(1,nfunc);        % Generate amplitude weight matrix
for i = 1:1:nfunc
%    amp(i) = 100000/(i^3); 
%    amp(i) = (i)/100; 
    amp(i) = 100/(i); 
end
iamp = find(amp<1);
amp(iamp) = 1;
pr = zeros(1,numel(time));

for i = 1:1:nfunc
   pr = pr + amp(i)*cos(freqs(i)*time + phases(i));
end
figure
plot(time, pr), grid on
title('Time Function')
xlabel('Time [s]')
ylabel('pr')
axis([0 max(time) min(pr) max(pr)])

Nfft = length(pr)-1;
fvals = (-Nfft/2:Nfft/2-1)/Nfft;
pr_fft = 2/Nfft*fft(pr, Nfft);  % Calculate fft and normalize by 2/Nfft
pr_fft_D = fftshift(pr_fft);

figure
plot(fvals, abs(pr_fft_D)), grid on
title('Double Sided FFT of Psuedo Random Excitation')
ylabel('DFT Values [mag]')
xlabel('Normalized Frequency')
% axis([0 numel(time) 0 max(abs(pr_fft_D))])
figure
plot(fvals, abs(20*log10(pr_fft_D))), grid on
title('Double Sided FFT of Psuedo Random Excitation')
ylabel('DFT Values [dB]')
xlabel('Normalized Frequency')
% axis([0 numel(time) 0 max(abs(20*log10(pr_fft_D)))])

% -- Simulate the system -- 
[ypr,~,xpr] = lsim(sys_cl,pr,time);

% -- Calculate control effort -- 
u_pr = -K_f*xpr' + F*pr;

% -- Plot Simulation Results -- 
label_p = 'Psuedo Random Response: No Noise';
hold on, grid on
% simplot2(time, pr, ypr, xpr, time, u_pr, label_p, 0, 0, nStates, nOut)

% -- Add noise to u --
n_rad = .0349;
pr_awgn = [pr' awgn(zeros(1,length(pr(1,:))),n_rad)']';

% -- Define CL matrices -- 
Fn = [F, 1];    % New F matrix to include noise
Bcln = B*Fn;
sys_cln = ss(Acl,Bcln,C,D,'statename',states,'inputname',inputs,'outputname',outputs);

% -- Simulate the system -- 
if o_data_flag == 0
    [yprn,~,xprn] = lsim(sys_cln,pr_awgn,time);
else
    load(matFile2use)
end

% -- Calculate control effort -- 
% Do NOT use the noise, the whole point is not knowing it!
uprn = -K_f*xprn' + F*pr;

yetfe = K_f*xprn';
uetfe = uprn;

% -- Plot Simulation Results -- 
label_p = 'Psuedo Random Response: Noise';
hold on, grid on
% simplot2(time, pr_awgn, yprn, xprn, time, uprn, label_p, 0, 0, nStates, nOut)

% -- Calculate the ETFE for 1st, 3rd and last N-sample segment --
% Get indices of N-segments
N1 = [1:N];
N3 = [2*N+1:3*N];
NL = [(length(pr)/N-1)*N:(length(pr)/N)*N];
NL = round(NL);

y1 = yetfe(N1);
u1 = uetfe(N1);

y3 = yetfe(N3);
u3 = uetfe(N3);

yL = yetfe(NL);
uL = uetfe(NL);

% -- Calculate fft's of u and y for N sections
% --
Nfft_1 = length(y1);
fvals_y1 = [-flip(freqs) freqs];
if o_data_flag == 0
    y1_fft = 2/Nfft_1*fft(y1, Nfft_1);  % Calculate fft and normalize by 2/Nfft
else
%     load('fft_n1_data.mat')
end
    
    
y1_fft_D = fftshift(y1_fft);
fvals_u1 = [-flip(freqs) freqs];
if o_data_flag == 0
    u1_fft = 2/Nfft_1*fft(u1, Nfft_1);  % Calculate fft and normalize by 2/Nfft
end
    
u1_fft_D = fftshift(u1_fft);

% -- Deal with 0th index of fft normalization --
if dc_flag == 1
    y1_fft_D = [y1_fft_D(1:100) y1_fft_D(102:end)];
    fvals_y1 = [fvals_y1(1:100) fvals_y1(102:end)];
    u1_fft_D = [u1_fft_D(1:100) u1_fft_D(102:end)];
    fvals_u1 = [fvals_u1(1:100) fvals_u1(102:end)];
    disp('shouldnt be here')
end

% --
Nfft_3 = length(y3);
fvals_y3 = [-flip(freqs) freqs];
if o_data_flag == 0
    y3_fft = 2/Nfft_3*fft(y3, Nfft_3);  % Calculate fft and normalize by 2/Nfft
else
%     load('fft_n3_data.mat')
end
y3_fft_D = fftshift(y3_fft);

fvals_u3 = [-flip(freqs) freqs];
if o_data_flag == 0
    u3_fft = 2/Nfft_3*fft(u3, Nfft_3);  % Calculate fft and normalize by 2/Nfft
end

u3_fft_D = fftshift(u3_fft);

% -- Deal with 0th index of fft normalization --
if dc_flag == 1
    y3_fft_D = [y3_fft_D(1:100) y3_fft_D(102:end)];
    fvals_y3 = [fvals_y3(1:100) fvals_y3(102:end)];
    u3_fft_D = [u3_fft_D(1:100) u3_fft_D(102:end)];
    fvals_u3 = [fvals_u3(1:100) fvals_u3(102:end)];
end

% --
Nfft_L = length(yL)-1;
% fvals_yL = (-Nfft_yL/2:Nfft_yL/2-1)/Nfft_yL;
fvals_yL = [-flip(freqs) freqs];
if o_data_flag == 0
    yL_fft = 2/Nfft_L*fft(yL, Nfft_L);  % Calculate fft and normalize by 2/Nfft
else
%     load('fft_nL_data.mat')
end
    
yL_fft_D = fftshift(yL_fft);
fvals_uL = [-flip(freqs) freqs];
if o_data_flag == 0    
    uL_fft = 2/Nfft_L*fft(uL, Nfft_L);  % Calculate fft and normalize by 2/Nfft
end
uL_fft_D = fftshift(uL_fft);

% -- Deal with 0th index of fft normalization --
if dc_flag == 1
    yL_fft_D = [yL_fft_D(1:100) yL_fft_D(102:end)];
    fvals_yL = [fvals_yL(1:100) fvals_yL(102:end)];
    uL_fft_D = [uL_fft_D(1:100) uL_fft_D(102:end)];
    fvals_uL = [fvals_uL(1:100) fvals_uL(102:end)];
end

% Fft of y and u
figure
subplot(2,1,2)
plot(fvals_y1, abs(y1_fft_D)), grid on
title('Double Sided FFT of y Section 1')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')
subplot(2,1,1)
plot(fvals_u1, abs(u1_fft_D)), grid on
title('Double Sided FFT of u Section 1')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')

figure
subplot(2,1,2)
plot(fvals_y3, abs(y3_fft_D)), grid on
title('Double Sided FFT of y Section 3')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')
subplot(2,1,1)
plot(fvals_u3, abs(u3_fft_D)), grid on
title('Double Sided FFT of u Section 3')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')

figure
subplot(2,1,2)
plot(fvals_yL, abs(yL_fft_D)), grid on
title('Double Sided FFT of y Section L')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')
subplot(2,1,1)
plot(fvals_uL, abs(uL_fft_D)), grid on
title('Double Sided FFT of u Section L')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')

T1 = y1_fft(1:(N/2))./u1_fft(1:(N/2));
w1 = dw:dw:length(T1)*dw;

T3 = y3_fft(1:(N/2))./u3_fft(1:(N/2));
w3 = dw:dw:length(T3)*dw;

TL = yL_fft(1:(N/2))./uL_fft(1:(N/2));
wL = dw:dw:length(TL)*dw;

% -- Plot N-Segment ETFE's --
a1 = [1/divisor ubound -50 75];
a2 = [a1(1) a1(2) -125 0];

bodeworksplot(w, m, p, e, sig, w1, T1, a1, a2, ' 1')

bodeworksplot(w, m, p, e, sig, w3, T3, a1, a2, ' 3')

bodeworksplot(w, m, p, e, sig, wL, TL, a1, a2, ' Last')

%% 13. ETFE Error
disp('---------------- 13. ETFE Error --------------')

% TL_trim = TL(1:end-1);
TL_trim = TL;
error1 = abs((abs(TL_trim)-abs(T1))./abs(TL_trim)).*100;
% TL_trim = TL_trim(1:end-1);
error3 = abs((abs(TL_trim)-abs(T3))./abs(TL_trim)).*100;
figure
subplot(2,1,1)
% wL_trim = wL(1:end-1);
wL_trim = wL;
semilogx(wL_trim, error1), grid on
axis([10^-3 10^1 0 max(error1)])
ylabel('Error [%]')
xlabel('Frequency [rad/s]')
title('Error between N-1 & N-Last')
subplot(2,1,2)
% wL_trim = wL_trim(1:end-1);
wL_trim = wL;
semilogx(wL_trim, error3), grid on
ylabel('Error [%]')
xlabel('Frequency [rad/s]')
title('Error between N-3 & N-Last')
axis([10^-3 10^1 0 max(error3)])
%% 14. ETFE Average
disp('---------------- 14. ETFE Average --------------')

% Get indices of N-segments
for i = 2:1:round(length(pr)/N)-1
    Ntmp = (i*N+1:(i+1)*N)';
    Na(i-1,:) = Ntmp(1:N/2);
end
% Initialize input and output average variables
ya = zeros(1,N/2);
ua = zeros(1,N/2);

for i = 1:1:numel(Na(:,1))
    ya = ya + yprn(Na(i,:));
    ua = ua + uprn(Na(i,:));
end
ya = ya/i;
ua = ua/i;

% -- Calculate fft's of u and y for N sections
% --
Nfft_ya = length(ya)-1;
fvals_ya = (-Nfft_ya/2:Nfft_ya/2-1)/Nfft_ya;
ya_fft = 2/Nfft_ya*fft(ya, Nfft_ya);  % Calculate fft and normalize by 2/Nfft
ya_fft_D = fftshift(ya_fft);

Nfft_ua = length(ua)-1;
fvals_ua = (-Nfft_ua/2:Nfft_ua/2-1)/Nfft_ua;
ua_fft = 2/Nfft_ua*fft(ua, Nfft_ua);  % Calculate fft and normalize by 2/Nfft
ua_fft_D = fftshift(ua_fft);

% Deal with 0th index of fft normalization
ya_fft_D = [ya_fft_D(1:100) ya_fft_D(102:end)];
fvals_ya = [fvals_ya(1:100) fvals_ya(102:end)];
ua_fft_D = [ua_fft_D(1:100) ua_fft_D(102:end)];
fvals_ua = [fvals_ua(1:100) fvals_ua(102:end)];

% Calculate average ETFE 
Ta = ya_fft./ua_fft;
wa = dw:dw:length(Ta)*dw;

bodeworksplot(w, m, p, e, sig, wa, Ta, a1, a2, ' Averaged')

%% 15. ETFE Nyquist
disp('---------------- 15. ETFE Nyquist --------------')

re_etfe = real(TL);
im_etfe = imag(TL);

figure
subplot(2,1,1)
h1 = plot(re_etfe, im_etfe, 'b'); hold on, grid on
plot(re_etfe, -im_etfe, 'b')
h2 = plot(re, im, 'r');
plot(re, -im, 'r')
plot(-1, 0, 'r+')
title('Full Nyquist')
legend([h1, h2], 'ETFE', 'Model')
axis([-50, 2300, -1500, 1500])
subplot(2,1,2)
h8 = plot(re_etfe, im_etfe, 'b'); hold on, grid on
h9 = plot(re_etfe, -im_etfe, 'b');
h1 = plot(re, im, 'r');
h6 = plot(re, -im, 'r');
h2 = plot(x0,y0,'r--');
h3 = plot(x0,-y0,'r--');
h4 = plot(x,y,'k--');
h5 = plot(x,-y,'k--');
h7 = plot(-1, 0, 'r+');
h8 = plot(re_etfe, im_etfe, 'b');
h9 = plot(re_etfe, -im_etfe, 'b');
axis([-1.5, 2.5, -4, 4])
title('Zoomed in Nyquist')
legend([h1, h8, h2, h4],'Model', 'ETFE', '\delta0', '\deltaf')

%% 16. Parameterized Model
disp('---------------- 16. Parameterized Model --------------')

% Extract half of the fft data for the last N-section
yp = yL_fft(1:length(freqs));
up = uL_fft(1:length(freqs));
% wp = (2*pi/T*(0:1:(N/2)));    % Define fft frequencies
wp = freqs;                     % Which should end up just being the original frequencies

yp = yp(2:end);
up = up(2:end);
wp = wp(2:end);

% -- Weight frequencies --
dum = 10000;
amp(101:end) = dum;
    % amp(283+90:1001+90) = dum*100000000000;
amp(283:end) = dum*100000000000;

% amp(1:100) = wp(1:100)/1000;
% amp = wp*10000000;

% -- Choose order of poles and zeros for the parametric model --
op = 16; % 9 seem best, 18 from Lg, 16 from minreal(Lg), 15 best?
oz = 15; % 6 seem best, 17 from Lg, 15 from minreal(Lg), 12 best?

% --  Calculate x vector for parametric mode -- 
xp = zeros(length(wp),(op+oz+1));

k = 1;
for i = op:-1:1
    n = i;
    xp(:,k) = yp.*((wp.*1i).^(-n));
    k = k+1;
end

for i = 1:1:oz+1    % mm = m used mm because I already used m
    mm = i-1;
    xp(:,op+i) = up.*((wp.*1i).^(mm-op)); 
end
xp = xp.';

yp = yp.*amp;
xp = xp.*amp;

% -- Calculate optimum parameter vector to cost function J --
    % *note, ' operator = hermetian
    % .' = transpose
theta_star = real((yp.')'*transpose(xp))/real((conj(xp)*transpose(xp)));

% - Create transfer function --
num = [flip(theta_star(op+1:end))].';
den = [1, -flip(theta_star(1:op))].';

% -- Parametrized Bode --
sys_par = tf(num.', den.');
[m.mag_par, p.phase_par, w.w_par] = bode(sys_par, wp);
m.mag_par = squeeze(m.mag_par);
p.phase_par = squeeze(p.phase_par);

figure
subplot(2,1,1)
h1 = semilogx(w.w_par, 20*log10(m.mag_par), 'r'); hold on, grid on
h2 = semilogx(w.w_b, 20*log10(m.mag_b), 'b');
h3 = semilogx(e.epsilonx, e.epsilony, 'k--');
h4 = semilogx(w.w_b, sig.sigma, 'Color', [.5 .5 .5]);
h5 = semilogx(w.w_b, sig.sigmau, 'Color', [.5 .5 .5]);
legend([h1, h2, h3, h4],'Parametric', 'Truth', '\epsilon Bnd', '\sigma Bnds', 'Location', 'Northeast')
title(['Parametric Bode'])
xlabel('w[rad/s]')
ylabel('[dB]')
axis(a1)

subplot(2,1,2)
semilogx(w.w_par, p.phase_par, 'r'), hold on, grid on
semilogx(w.w_b, p.phase_b, 'b')
legend('Parametric', 'Truth', 'Location', 'Northeast')
ylabel('Phase [\circ]')
xlabel('\omega [rad/s]')
axis(a2)

% -- Parametrized Nyquist --
[re_par, im_par] = nyquist(sys_par);
re_par = squeeze(re_par);
im_par = squeeze(im_par);

figure
subplot(2,1,1)
h1 = plot(re_par, im_par, 'b'); hold on, grid on
plot(re_par, -im_par, 'b')
h2 = plot(re, im, 'r');
plot(re, -im, 'r')
plot(-1, 0, 'r+')
title('Full Nyquist')
legend([h1, h2], 'Parametric', 'Truth')
% axis([-50, 2300, -1500, 1500])
subplot(2,1,2)
h8 = plot(re_par, im_par, 'b'); hold on, grid on
h9 = plot(re_par, -im_par, 'b');
h1 = plot(re, im, 'r');
h6 = plot(re, -im, 'r');
h2 = plot(x0,y0,'r--');
h3 = plot(x0,-y0,'r--');
h4 = plot(x,y,'k--');
h5 = plot(x,-y,'k--');
h7 = plot(-1, 0, 'r+');
h8 = plot(re_par, im_par, 'b');
h9 = plot(re_par, -im_par, 'b');
% axis([-1.5, 2.5, -4, 4])
title('Zoomed in Nyquist')
legend([h1, h8, h2, h4],'Truth', 'Parametric', '\delta0', '\deltaf')

figure
step(feedback(sys_par,1), 'r'), hold on, grid on
step(feedback(Lg,1), 'b--')
axis([0, 100, 0, 1.2])
title('Parametric Model Step Response')
legend('Parametric', 'Nominal Lg') 

% figure
% pzmap(sys_par)

%% 17. Simulate ETFE/Parameterized Model
disp('---------------- 17. Simulate ETFE/Parameterized Model --------------')

yL_trim = yL(1:(N/2));
uL_trim = uL(1:(N/2));

data = iddata(yL_trim', uL_trim' ,dt);

sys_etfe = etfe(data, 100);
sys_etfe_imp = impulseest(data);

figure
step(feedback(sys_etfe_imp,1), 'r'), hold on, grid on
step(feedback(Lg,1), 'b--')
axis([0, 100, 0, 1.2])
title('ETFE Step Response')
legend('ETFE', 'Nominal Lg') 

% figure
% step(feedback(sys_etfe,1),'r'), hold on, grid on
% step(feedback(Lg,1),'b--')
% axis([0, 14, 0, 1.2])
% title('ETFE Step Response (etfe)')
% legend('ETFE', 'Nominal Lg') 

end % end sysID block
