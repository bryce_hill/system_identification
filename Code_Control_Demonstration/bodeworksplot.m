function bodeworksplot(w, m, p, e, sig, wp, T, a1, a2, Title)

figure
subplot(2,1,1)
h7 = semilogx(wp, 20*log10(abs(T)), 'b'); hold on, grid on
h1 = semilogx(w.w_b, 20*log10(m.mag_b), 'r');
h2 = semilogx(w.w_b_high, 20*log10(m.mag_b_high), 'c--');
h3 = semilogx(w.w_b_low, 20*log10(m.mag_b_low), 'c--');
h4 = semilogx(e.epsilonx, e.epsilony, 'k--');
h5 = semilogx(w.w_b, sig.sigma, 'Color', [.5 .5 .5]);
h6 = semilogx(w.w_b, sig.sigmau, 'Color', [.5 .5 .5]);
axis(a1)
legend([h1, h7, h4, h5],'Model', 'ETFE', '\epsilon Bnd', '\sigma Bnd', 'Location','Northeast')
title(['N-segment', Title])
xlabel('w[rad/s]')
ylabel('[dB]')

subplot(2,1,2)
semilogx(wp, 180/pi*angle(T), 'b'), hold on, grid on
semilogx(w.w_b, p.phase_b, 'r')
% semilogx(w_b, sigmap, 'r--')
% semilogx(w.w_b_high, p.phase_b_high, 'c--')
% semilogx(w.w_b_low, p.phase_b_low, 'c--')
% semilogx(w_b, sigmaup, 'r--')
% legend('Bode', '\sigma Bnd', 'U/L Gain Bnd')
axis(a2)
legend('ETFE', 'Model','Location','Northeast')
title(['Performance Bounds ', Title])
ylabel('Phase [\circ]')
xlabel('\omega [rad/s]')

end