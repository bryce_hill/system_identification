function bounds(t, r, b)
% Class:      ASEN 6519 - System Identification
% 
% File:       b.bounds.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    plot reference trajectory and constraints

% plot(t, r(:,2), '--')
hold on, grid on

if size(r,2) == 2
    plot(t, r(:,2), '--')
    plot(t, zeros(length(t),1),'--m')
    plot(t, (r(:,2)+b.bound'), '--r')
    plot([t(b.b1) t(b.b1)], [(1-b.bound(1))*r(b.b1,2) (1+b.bound(1))*r(b.b1,2)],'c')
    plot(t, (r(:,2)-b.bound'), '--r')
    plot([t(b.b2) t(b.b2)], [r(b.b2,2)-b.bound(1) r(b.b2,2)+b.bound(1)],'c')
    plot([t(b.b3) t(b.b3)], [r(b.b3,2)-b.bound(1) r(b.b3,2)+b.bound(1)],'c')
elseif size(r,2) == 1
    plot(t, r(:), '--')
    plot(t, zeros(length(t),1),'--m')
    plot(t, (r+b.bound'), '--r')
    plot([t(b.b1) t(b.b1)], [(1-b.bound(1))*r(b.b1) (1+b.bound(1))*r(b.b1)],'c')
    plot(t, (r-b.bound'), '--r')
    plot([t(b.b2) t(b.b2)], [r(b.b2)-b.bound(1) r(b.b2)+b.bound(1)],'c')
    plot([t(b.b3) t(b.b3)], [r(b.b3)-b.bound(1) r(b.b3)+b.bound(1)],'c')
end

legend('Altitude Position Reference [m]','X Pose Reference [m]','20% Overshoot Req','10(s) Settling Req', 'Location', 'northwest')
axis([0 100 -.5 4])

end