function e = errorcalcs(index, y, r, wOut, label_p)
% Class:      ASEN 6519 - System Identification
% 
% File:       performance.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Calculate settling time
if size(r,2) == 2
    e = 100*(abs(y(index,wOut) - r(index,wOut))/r(index,wOut));
elseif size(r,2) == 1
    e = 100*(abs(y(index,wOut) - r(index))/r(index));
end
display([label_p, ' Output #', num2str(wOut) ,' % Error = ', num2str(e)])
end