function simplot2(t, r, y_hist, x_hist, t_hist, u_hist, title_p, obsv, integ, nStates, nOut)
% Class:      ASEN 6519 - System Identification
% 
% File:       performance.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Plot y, r, actuator effor u, system, integral and
%               observer states

if obsv == 1 && integ == 1
    nSub = 3;
elseif obsv ==1 || integ == 1
    nSub = 2;
else 
    nSub = 2;
end
    
% Plot the output of the system vs. reference command
figure
subplot(2,nSub,1)

% h1 = plot(t_hist, y_hist(:,1));
% h2 = plot(t_hist, y_hist(:,2));
% h3 = plot(t_hist, r(:,2), '--');
hold on
plot(t_hist, y_hist(:,1));
plot(t_hist, y_hist(:,2));
% plot(t_hist, r(:,2), '--');
plot(t_hist, r, '--');
grid on
% bounds(t, r, b)
title(['Tracking vs R (', title_p, ')'])
% legend([h1 h2 h3],'\deltaX pose','\deltaZ Pose', 'Ref. \deltaZ Input')
legend('\deltaX pose','\deltaZ Pose', 'Ref. \deltaZ Input')

% Plot states vs R
% figure
subplot(2,nSub,2)
plot(t_hist, x_hist(:, 1:nStates)),  hold on, grid on
title(['States vs Time (', title_p, ')'])
legend('\deltau (v_x)','\deltaw (v_z)','\deltaq (pitch rate)','\delta\theta (pitch \angle)','\deltax (pose)','\deltaz (pose)','Location','northwest')
xlabel('Time [s]')


% Plot Actuator effort and bounds
if nSub == 2
    subplot(2,nSub,3)
else
    subplot(2,nSub,4)
end
bnd = t_hist;
bnd(:) = .5236;
% figure
plot(t_hist,u_hist(1,:))
hold on
% plot(t_hist,u_hist(2,:),'k')
plot(t_hist, bnd,'--r')
plot(t_hist, -bnd,'--r')
grid on
% legend('Elevator','Thrust','Limit 30 \circ')
legend('Elevator','Limit 30 \circ')
title(['Actuator Effort vs. Time (', title_p, ')'])
xlabel('Time(s)')
ylabel('Deflection (rad)')

% Plot Integral States
if integ == 1
    if nSub == 2
        subplot(2,nSub,4)
    else
        subplot(2,nSub,5)
    end
    plot(t_hist, x_hist(:,nStates+1:nStates+nOut)), hold on, grid on;
    title(['Integral States vs. Time (', title_p, ')'])
    xlabel('Time(s)')
    legend('Err: X Pose', 'Err: Z Pose','Location','northwest')
end

% Plot Observer States
if obsv == 1
    subplot(2,nSub,3)
    if obsv == 1 && integ ~= 1
        plot(t_hist, x_hist(:,nStates+1:nStates+nStates)),  hold on, grid on;
    elseif obsv ==1 && integ == 1
        plot(t_hist, x_hist(:,nStates+nOut+1:nStates+nOut+nStates)),  hold on, grid on;
    end
    title(['Observer States vs. Time (', title_p, ')'])
    xlabel('Time [s]')
    legend('Obsv: \deltau (v_x)','Obsv: \deltaw (v_z)','Obsv: \deltaq (pitch rate)','Obsv: \delta\theta (pitch \angle)','Obsv: \deltax (pose)','Obsv: \deltaz (pose)','Location','northwest')
end  
    

% % Plot both observer and integral states together
% if obsv == 1 || integ == 1
%     subplot(2,2,4)
%     hold on, grid on
%     if integ == 1
%         h2 = plot(t_hist, x_hist(:,nStates+1:nStates+nOut)), hold on, grid on;
%     end    
%     if obsv == 1 && integ ~= 1
%         h1 = plot(t_hist, x_hist(:,nStates+1:nStates+nStates)),  hold on, grid on;
%     elseif obsv ==1 && integ == 1
%         h1 = plot(t_hist, x_hist(:,nStates+nOut+1:nStates+nOut+nStates)),  hold on, grid on;
%     end
%         
% elseif obsv == 1
%         legend([h1],'Obsv: \deltau (v_x)','Obsv: \deltaw (v_z)','Obsv: \deltaq (pitch rate)','Obsv: \delta\theta (pitch \angle)','Obsv: \deltax (pose)','Obsv: \deltaz (pose)','Obsv: Unknown 1','Obsv: Unknown 2','Location','northwest')
%     elseif integ == 1
%         legend([h2], 'Err: X Pose', 'Err: Y Pose', 'Err: Servo','Location','northwest')
%     end
%     title(['States vs Time (', title_p, ')'])
%     xlabel('Time [s]')
% end

end