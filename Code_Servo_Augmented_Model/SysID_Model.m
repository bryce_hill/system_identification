% Class:      ASEN 6519 - System Identification
% 
% File:       SysId_Model
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Longitudinal Aircraft Model simulation

close all, clear all, clc
%% 0. State Space Model
disp('-------------- 0. Model Definition --------------')

% State vector x = [u w q theta x z] 
% where: 
% u = perturbed inertial X velocity component
% w = perturbed inertial Z velocity component
% q = perturbed Y rotational rate
% theta = perturbed pitch angle
% x = position in inertial X direction
% z = position in inertial Z direction
 
A = [-.3206 .3514   0       -9.8048 0   0;
     -.9338 -6.8632 19.8386 -.3187  0   -.0004;
     0      -2.5316 -3.9277 0       0   0;
     0      0       1       0       0   0;
     1      0       0       0       -.1 0;
     0      1       0       -21     0   0];
 
B = [.0037  11.2479;
     -.1567 0;
     -.9332 0;
     0      0;
     0      0;
     0      0];
% B = B(:,1);

% Input vector u = [elevator; throttle];
C = [0 0 0 0 1 0;
     0 0 0 0 0 1];

out1 = 'X pose';
out2 = 'Z pose';

D = [0];

% Get servo model
sys_servo = servomodel(2);

% Augment model to incorporate servo dynamics
As = sys_servo.A;
Bs = sys_servo.B;
Cs = sys_servo.C;
Ds = sys_servo.D;
% C_star = [zeros(1,size(C,2)) Cs]

A = [A                           B*Cs;
     zeros(size(As,1),size(A,1)) As];
B = [zeros(size(B,1),size(Bs,2));
     Bs];
% B = [zeros(size(B)),              zeros(size(B,1),size(Bs,2));
%      zeros(size(Bs,1),size(B,2)), Bs];
% C = [C                           zeros(size(C,1),size(Cs,2));
%      zeros(size(Cs,1),size(C,2)) Cs];
C = [C zeros(size(C,1),size(Cs,2))];

D = [0];
 
sys_ol = ss(A,B,C,D); 

nStates = length(C)
nOut = size(C,1)
nIn = size(B,2)

%% 1. Reference definition
disp('-------------- 1. Reference Definition --------------')
% = 22;   %[m/s] true air speed
waypoints = [1, 2, 3];
time = 100;
dt = .01;
t = 0:dt:time;
t1 = 10/dt;
t2 = 35/dt;
t3 = 60/dt;
r = t;
r(1:t1) = 0;
r(t1:t2) = waypoints(1);
r(t2:t3) = waypoints(2);
r(t3:end) = waypoints(3);

% TODO - am I stepping throttle or elevator??
r =[zeros(length(t),1) ,r(:)]; %step on Z perturbation

% Each waypoint is a 1 meter step. Set bount to be .2 (20%) of step
b.bound = t;
b.bound(:) = .2;  % 20% 
ts = 10;
% Bound idices
b.b1 = t1+ts/dt;
b.b2 = t2+ts/dt;
b.b3 = t3+ts/dt;
% figure
% Call bounds.m script to plot reference and bounds
% bounds
% title('Reference and Constraints')
% xlabel('Time [s]')

%% 2. Modes, Eigenspaces, Lyapunov/Asymptotical Stability
disp('-------------- 2. Modes, Eigenspaces, Lyapunov/Asymptotical Stability --------------')
% -- Eigen values -- 
[eigV, eigD] = eig(A);
poles_ol = diag(eigD)
% -- Eigencoordinate representation -- 
    % T = eigV;
    % LAMBDA = inv(T)*A*T;
    % GAMMA = inv(T)*B;
    % sys_eig = ss(LAMBDA,GAMMA,C,D);

% -- Simulate --
times = 35;
ts = 0:.1:times;

% figure
% rz = zeros(numel(ts), nOut);
% y_ol_sim = zeros(nStates*nOut,numel(ts))';
% t_ol_sim = zeros(1, numel(ts));
% x0 = zeros(nStates, 1);
% perturbation = .5;
% j = 1;  % Index for subplots, increments by #outputs
% for i = 1:1:nStates
%     x0(:) = 0;              % Zero out initial conditions vector
%     x0(i) = perturbation;   % Single state perturbation
%     [y_ol_sim(:,j:j+1), t_ol_sim] = lsim(sys_eig, rz', ts, x0);    % Simulate System Response
%     subplot(6,2,j)
%     plot(t_ol_sim, y_ol_sim(:,j)), xlabel('Time [s]'), ylabel(out1), title(['State', num2str(i), ': ', 'Output 1', '| Perturbation = ',num2str(perturbation)]), grid on
%     subplot(6,2,j+1)
%     plot(t_ol_sim, y_ol_sim(:,j+1), 'r'), xlabel('Time [s]'), ylabel(out2), title(['State', num2str(i), ': ' , 'Output 2', '| Perturbation = ',num2str(perturbation)]), grid on
%     j = i*nOut+1;
% end
    
    
%% 3. Reachability & Controllability 
disp('-------------- 3. Reachability % Controllability --------------')

% -- Assess controlability/reachability -- 
rankCont = rank(ctrb(A,B))

% -- Assess the observability of this system -- 
rankObsv = rank(obsv(A,C))

%% 4. State Feedback Controller
    % TODO - why do I still see discrete actuator jumps here?
disp('-------------- 4. State Feedback Controller --------------')
% -- Determine Desired Poles -- 
perturbations = [0; -0.11; -0.11; -.01; -.01; -1; -2; -.1; 0; 0];
poles_cl = perturbations + poles_ol

% -- Use place command to generate K matrix -- 
K_f = place(A,B,poles_cl)

% F = inv(C/(-A+B*K)*B)   % DC Tracking controller
% F1 = eye(2);              % We did this in a  homework
% F = (D-1)/(C/(A-B*K)*B)   % No steady state error
% F = -inv(C/(A-B*K)*B)
% Use least squares solution to H*F = eye(3) where H = C*(-A+B*K)^-1*B
% H = C/(-A+B*K_f)*B
% F = inv(H'*H)*H'*eye(1)

% G = C/(-A+B*K_f)*B;
F = inv(C/(-A+B*K_f)*B)


% -- Define CL matrices -- 
Acl = A - B*K_f;
Bcl = B*F;
sys_cl = ss(Acl,Bcl,C,D);

% -- Closed loop eigenvectors -- 
[V_cl, d_cl] = eig(Acl);
eig_cl = diag(d_cl)

% -- Simulate the system -- 
[yfcl,~,xfcl] = lsim(sys_cl,r,t);

% -- Calculate control effort -- 
u_f = -K_f*xfcl' + F*r';

% -- Plot Simulation Results -- 
label_p = 'Feedback';
hold on, grid on
simplot(t, r, yfcl, xfcl, t, u_f, label_p, 0, 0, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yfcl, r, 2, label_p);

%% 5. Integral control
disp('---------------- 5. Integral Control --------------')
Aa = [A zeros(nStates,nIn);
     -C zeros(nOut,nIn)]
Ba = [B; zeros(nIn,nIn)]
Fa = [zeros(size(B)); eye(nIn)]
Ca = [C zeros(nOut,nIn)]
Da = 0;

% TODO - do these values make sense?
dPole_i = [poles_cl; -1; 0]

% -- Calculate new K matrix -- 
Ka = place(Aa,Ba,dPole_i);
AaCL = Aa - Ba*Ka;
BaCL = Fa;
eigIcl = eig(AaCL);

% -- Is the system controllable? -- 
rank = rank(ctrb(AaCL,BaCL))

% -- Form new SS system -- 
sysiCl = ss(AaCL, BaCL, Ca, Da);

% -- Simulate with first reference input -- 
[yicl,~,xicl] = lsim(sysiCl,r,t);

% -- Calculate actuator effort: -- 
u_i = -Ka*xicl';

% -- Plot Simulation Results -- 
label_p = 'Integral';
hold on, grid on
simplot(t, r, yicl, xicl, t, u_i, label_p, 0, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yicl, r, 2, label_p);

%% 6. Observer + Integral
disp('---------------- 6. Integral + Observer --------------')
% My system originally has 6 states. Add 2 for integral control and 6 more
% to reconstruct my states for the observer. 
% Observer states = nStates + nIntegral(=#outputs) + nStates 

% Choose observer poles
t_factor = 10000;      % Tuning Factor
dpoles_o = t_factor.*poles_cl

% Use place command to get Luenberger observer gain via dual system:
L = (place(A',C',dpoles_o))';

Fo = [     zeros(size(B)); 
                eye(nIn);
      zeros(nStates,nIn)];                              % augment for observer error states
Ao = [Aa - Ba*Ka,                 Ba*Ka(:,1:nStates);   % Ignore K integral terms
      zeros(nStates,nStates+nIn), A-L*C];               % nIn here maybe should be replaced by number of integral additions?
Bo = Fo;
Co = [C zeros(nOut,nStates+nOut)];
Do = zeros(size(Co,1),size(Bo,2));

xoIC = zeros*ones(size(Ao,1),1);
% xoIC = [.05 .05 .05 .0175 .0175 .1 .1 .1 .1 .1 .1 .1 .1 .1]
% xoIC = [1 .05 .05 .0349 .0349 .01 .01 .01 0 -.01 .01 0 0 .01]
syso = ss(Ao,Bo,Co,Do);
[yio, ~, xio] = lsim(syso,r,t,xoIC);

% -- Calculate actuator effort --
u_io = -[Ka Ka(:,1:nStates)]*xio';

% -- Plot Simulation Results -- 
label_p = 'Observer + Integral';
hold on, grid on
simplot(t, r, yio, xio, t, u_io, label_p, 1, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, yio, r, 2, label_p);

%% 7. LQR + integral
disp('---------------- 7. LQR + Integral --------------')
% Before I can add the observer, design LQR + integral
% Define open loop augmented state matrices
Aaol = [A zeros(nStates,nOut); -C zeros(nOut,nOut)];
Baol = [B; zeros(nIn,nIn)];
Faol = [zeros(size(B)); eye(nIn)];
Caol = [C zeros(nOut)];
Daol = 0;
aolsys = ss(Aaol,Baol,Caol,Daol);

% TODO - tune

% Define alpha, 6 states + 2 error(integral)
alpha = [1/1000 1 1/1000000 1 1 1/100 1/100 1 1 1 .0001]
% Scale alpha values
alpha = alpha./sum(alpha);
% Define Q state deviation penalization matrix
    % The maximum state error defined in the LQR section are as follows:
    % Y vel, roll rate, yaw rate, roll angle, yaw angle, lateral pose
    % 10^2   .5^2         .5^2      .349^2    .349^2   .01^2 
Qa = diag(alpha./[1^2 1^2 1^2 1^2 1^2 1^2 1^2 1^2 1^2 1^2 1^2]);

beta = 1;
rho = 100000;
% Actuator constraints
ucon = .5236;    % [rad] = 30 degrees

% TODO - tune this
% Define R actuator penalization matrix 
Ra = rho*diag([.5/ucon^2 .5/ucon^2 .5/ucon^2]);

[Ka_lqr,Wa,clEvalsAug] = lqr(aolsys,Qa,Ra);

Facl = Faol;
Aacl = Aaol - Baol*Ka_lqr;
eigacl = eig(Aacl)
Bacl = Facl;
sys_lqr = ss(Aacl,Bacl,Caol,Daol);
[y_lqr_cl, ~, x_lqr_cl] = lsim(sys_lqr,r,t);
u_lqr = -Ka_lqr*x_lqr_cl';

% figure
% pzmap(sys_lqr)

% -- Plot Simulation Results -- 
label_p = 'LQR + Integral';
hold on, grid on
simplot(t, r, y_lqr_cl, x_lqr_cl, t, u_lqr, label_p, 0, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, y_lqr_cl, r, 2, label_p);

%% 8. LQR + Integral + Observer
disp('---------------- 8. LQR + Integral + Observer --------------')
% Define closed loop, integral, feeforward, LQR + observer state matrices
Faclo = [Faol; zeros(nStates,nOut)];
Aaclo = [Aaol - Baol*Ka,                  Baol*Ka(:,1:nStates); % Ignore Ki terms
             zeros(nStates,nStates+nOut), A-L*C];        
Baclo = Faclo;
Caclo = [Caol zeros(nOut,nStates)];
Daclo = zeros(size(Caclo,1),size(Baclo,2));

sys_lqri = ss(Aaclo,Baclo,Caclo,Daclo);
% xcloIC = .1*ones(14,1);

[y_lqri_cl, ~, x_lqri_cl] = lsim(sys_lqri,r,t,xoIC);

% Calculate actuator effort:
u_lqri = -[Ka_lqr Ka_lqr(:,1:nStates)]*x_lqri_cl'; % Ignoring integral states?

% -- Plot Simulation Results -- 
label_p = 'LQR + Integral + Observer';
hold on, grid on
simplot(t, r, y_lqri_cl, x_lqri_cl, t, u_lqri, label_p, 1, 1, nStates, nOut, b)
e_settime = errorcalcs(b.b1, y_lqri_cl, r, 2, label_p);

% figure
% bode(sys_lqri)