function e = errorcalcs(index, y, r, wOut, label_p)
% Class:      ASEN 6519 - System Identification
% 
% File:       performance.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Calculate settling time
% e = 100*(abs(y(index,wOut) - r(index,wOut))/r(index,wOut));
e = 100*(abs(y(index,wOut) - r(index))/r(index));
display([label_p, ' Output #', num2str(wOut) ,' % Error = ', num2str(e)])
end