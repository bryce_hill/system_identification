function [syscl] = servomodel(number)
% Class:      ASEN 6519 - System Identification
% 
% File:       servomodel.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Create servo model

disp('---- 0.1 Servo Model Parameters ---- ')

% Servo Model Parameters
s = tf('s');
Kt = .1;
Ka = .5;
Kf = 636;
Kdac = .00488;
J = 10^-4;
P = .01;
D = .1;
I = 1;

% Loop Transfer Function
    % L = (Ka*Kt)/(J*s^2)*Kf*(P+s*D)*Kdac
% num_L = Ka*Kt*Kf*Kdac.*[D P];
% den_L = J.*[1 0 0];
% [A_L, B_L, C_L, D_L] = tf2ss(num_L, den_L);
% sys_L = ss(A_L, B_L, C_L, D_L);

% Closed Loop Transfer Function
G = (D*s + P)*Kdac*Ka*Kt*(1/(J*s^2));
H = Kf;
Tcl = Kf*G/(1+G*H); % Multiply by Kf to get into the right units of rad2rad
Tcl = minreal(Tcl); % Was necessary to get rid of pole zero cancellation
[num, den] = tfdata(Tcl, 'v');

% num = Kt*Ka*Kdac.*[1/J D P]
% den = 1 + Kt*Ka*Kdac.*[1/J D P]*Kf
% num = [.244 48.8];
% den = [1 155.184 31036.8]

[A, B, C, D] = tf2ss(num, den);

if number == 2
% For now, use same servo model for throttle and elevator
    A = [A,             zeros(size(A));
         zeros(size(A)) A];
    B = [B,             zeros(size(B));
         zeros(size(B)) B];
    C = [C              zeros(size(C));
         zeros(size(C)) C];
end

syscl = ss(A, B, C, D);

% -- Assess controlability/reachability -- 
rankCont = rank(ctrb(A,B));
% -- Assess the observability of this system -- 
rankObsv = rank(obsv(A,C));

[eigV, eigD] = eig(A);
Servo_Poles = diag(eigD)

% Simulate Step Response
dt = .0001;
tlen = .1;
t = 0:dt:tlen;
r = ones(1, numel(t));
if number == 2
    r =[r(:), zeros(length(t),1)];
end

[y2, ~, ~] = lsim(syscl, r, t);
figure
plot(t, r,'--k')
hold on, grid on
plot(t, y2)
title('Step Response of Servo Model'), xlabel('Time [s]')

end



