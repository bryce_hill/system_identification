function [v_hist2, u_hist2, psit_hist, theta_hat_hist] = discrete_sim(theta_t, theta_s, n_index, m_index, i_ref, ssize, pparms, aparms)
%DISCRETE_SIM Simulate discrete system defined by numerator and denominator
% coefficients defined in theta
% 
%   INPUTS:
%   Theta   = [alpha_0, ..., alpha_n , beta_0, ... , beta_m]
%   n_index = Number of denominator coefficients
%   m_index = Number of numerator coefficients
%   i_ref   = reference input
%   time    = reference input time axis
%   ssize   = step size, if = 0, use least squares, else ssize =
%             gradient descent step size
%   pparms: plot parameters struct
%           pparms.splot = true/false to plot or not
%           pparms.sub_title = subtitle plot titles
%   aparms: algortithm parameters [F, dt, estimate]
%           F =  feedforward gain matrix (specific to this application)
%           dt = sampling period delta t [s]
%           estimate = true/false, turn on/off parameter estimation using
%                      gradient descent | least squares depending on ssize
% 
%   OUTPUTS:
%   v_hist          = Output history
%   u_hist          = Input history
%   psit_hist       = Data vector history
%   theta_hat_hist  = Parameter history

% For ease of reading, assign aparm and pparm parameters here
% ssize = aparms(1);
F = aparms(1);
dt = aparms(2);
estimate = aparms(3);
warning('off', 'MATLAB:nearlySingularMatrix')

% Use recursive least squares step size function
if ssize == 0
    warning('off', 'MATLAB:nearlySingularMatrix')
    R = 0;
    ssize = 0;              % Ensure ssize stays at zero until R is full rank
    least_squares = true;
    ill_conditioned = true; % Flag to wait until recursive least squares 
else                        % update is fullish rank (at least not ill
    least_squares = false;  % conditioned)
    ill_conditioned = false;
end

% -- The following is does the same but is implemented more like it would
%    be done on a microcontroller (excluding the data storage via hist vars

% Preallocate storage vectors
v_hist2 = [zeros(1, length(i_ref)-1)];
u_hist2 = [zeros(1, length(i_ref)-1)];
psit_hist = zeros(length(i_ref)-1, length(theta_t));
theta_tilda_hist = zeros(length(i_ref)-1, length(theta_t));
theta_hat_hist = zeros(length(i_ref)-1, length(theta_t));
e_vhist = zeros(1,length(i_ref)-1);

% Initialize variables
psit = zeros(1,length(theta_t));
theta_hat = theta_s;

for k = 1:1:length(i_ref)-1
% for k = 1:1:50
    
    % -- TRUTH --
    % Output Prediction v+1 and updated u+1
    v = psit*theta_t;
    u = F*i_ref(k+1) - v;

    % Calculate what will be the old psi
    psit = [v, psit(1:n_index), u, psit(m_index+1:end-1)];
    
    % Store the input and output
    v_hist2(k+1) = v; 
    u_hist2(k+1) = u;
    psit_hist(k, :) = psit;
    
    % -- PARAMETER ESTIMATE -- 
    if estimate == true
        theta_tilda = theta_t - theta_hat;              % Parameter error
%         disp(['t~ = ', num2str(size(theta_tilda))])
        e_v = psit*(theta_tilda);                       % Error between truth, model
%         disp(['size(e_v) = ', num2str(size(e_v))])
%         disp(['size(psit.T) = ', num2str(size(psit.'))])
        theta_hat = theta_hat + ssize*(psit.')*e_v;     % Parameter update (gradient descent)
        theta_tilda = theta_tilda - ssize*(psit.')*e_v; % Parameter error update
%         theta_tilda
%         disp(['size(ssize*psit*e_v) = ', num2str(size(ssize.*psit.'*e_v))])

        % Store parameters
        theta_tilda_hist(k, :) = theta_tilda;
        theta_hat_hist(k,:) = theta_hat;
        e_vhist(k) = e_v;
        
        % Least Squares Estimate
        if least_squares == true
            psit_squared_inv = inv(psit.'*psit);
            
            if k < 2
                [msg, id] = lastwarn;
                warning('off', id)
            end

%             R = R + inv(psit_squared)
            R_nan = any(isnan(psit_squared_inv(:)));
            R_inf = any(isinf(psit_squared_inv(:)));
            if R_nan == false && R_inf == false
%                 eig(psit_squared_inv)
%                 if min(eig(psit_squared_inv)) > .00001
                    ill_conditioned = false;
                    R = R + psit_squared_inv;
%                 end
            end
            if ill_conditioned == false
                ssize = R;
%                 k
            end
        end

        % DEBUG
        % disp(['v: ', num2str(v), ' | F: ', num2str(F), ' | i_ref(k+1) = ', num2str(i_ref(k+1)), ' | u: ', num2str(u)])
    end  
end

% -- Plot if splot flag is set -- 
if pparms.splot ~= false
    
    % Create time axes
    time_hist = (1:1:length(i_ref)-1)*dt;
    time_pr = (1:1:length(i_ref))*dt;
%     tdum = (1:1:length(v_hist))*dt;

    % Plot truth output & input
    figure
    subplot(2, 1, 1)
    plot(time_pr, i_ref, '--r'), hold on, grid on
    plot(time_pr, v_hist2, 'b')
    legend('Reference', 'v')
    title([pparms.sub_title, 'Discrete Time Sim Results'])
    xlabel('Time [s]')
    subplot(2,1,2)
    plot(time_pr, i_ref, 'r--'), hold on, grid on
    plot(time_pr, u_hist2, 'b'), grid on
    legend('Reference', 'u')
    
    if estimate == true
        % Plot parameters
        figure
        halfway = ceil(n_index/2);
        for n = 1:1:n_index+1
            subplot(halfway,2,n)
            plot(time_hist, theta_hat_hist(:,n)), grid on, hold on
            xx = [time_hist(1), time_hist(end)];
            yy = [theta_t(n), theta_t(n)];
            plot(xx, yy, '--r')

            error = 100*abs((theta_hat_hist(end,n)-theta_t(n))/theta_t(n));
            title(['\alpha n-', num2str(n)]);
            xlabel(['% SS error = ', num2str(error)])
            legend('Estimate', 'Truth')
        end
        subtitle([pparms.sub_title, ' \alpha Parameters']);

        figure
        for m = 1:1:m_index
            subplot(halfway,2,m)
            plot(time_hist, theta_hat_hist(:,n+m), 'b'), grid on, hold on
            xx = [time_hist(1), time_hist(end)];
            yy = [theta_t(n+m), theta_t(n+m)];
            plot(xx, yy, '--r')

            error = 100*abs((theta_hat_hist(end,n+m)-theta_t(n+m))/theta_t(n+m));
            title(['\beta m-', num2str(m)]);
            xlabel(['% SS error = ', num2str(error)])
            legend('Estimate', 'Truth')
        end  
        subtitle([pparms.sub_title, ' \beta Parameters']);
        
        figure
        % Plot parameter error norm vs iteration index
        parameter_error_norm = zeros(1,length(k));
        size(parameter_error_norm)
        for kk = 1:1:k
            parameter_error_norm(kk) = norm(psit_hist(kk,:),2);
        end
        size(parameter_error_norm)
        subplot(2,1,1)
        plot(1:1:length(parameter_error_norm), parameter_error_norm), grid on
        title([pparms.sub_title, ' Parameter Error Norm'])
        xlabel('Index')
        ylabel('Error Norm')
        
        subplot(2,1,2)
        plot(1:1:length(e_vhist), e_vhist), grid on
        title([pparms.sub_title, ' Output Prediction Error'])
        xlabel('Index')
        ylabel('Prediction Error')
    end
        
%     figure
%     plot(time, theta_tilda_hist), grid on
%     title('Parameter Error')
%     xlabel('Time [s]')
%     legend('\alpha_{n-1}', '\alpha_{n-2}', '\alpha_{n-3}', '\alpha_{n-4}', '\alpha_{n-5}', '\alpha_0', '\beta_m', '\beta_{m-1}', '\beta_{m-2}', '\beta_{m-3}', '\beta_{m-4}', '\beta_{m-5}','\beta_0')
warning('on', 'MATLAB:nearlySingularMatrix')
end










% -- This was the first way I implemented it. It is hard to get the indices
%    right. However, it makes sense from a simulation perspective. See
%    below for an implementation like what would be done for flight code
% Preallocate v output vector
% Add zeros to beginnings of vectors for the k < n/m cases (before anything
% has happened)
% v_hist = [zeros(1,n_index), zeros(1, length(i_ref))];
% u_hist = [zeros(1,m_index), i_ref];

% % Begin Simulation (max(n, m) becomes "0th" start index)
% for k = max(n_index, m_index):1:length(i_ref)-1
% % for k = max(n_index, m_index):1:100
%     % Prediction (output @ k+1)
%     psit = [flip(v_hist(k-n_index: k)), flip(u_hist(k-n_index: k+m_index-n_index-1))];
%     
%     % Output prediction 
%     v_hist(k+1) = psit*theta_t;
%     
%     % Input Update (Simulating in closed loop, pass pseudo through F
%     u_hist(k+1) = F*i_ref(k+1) - v_hist(k+1);
%     
%     % DEBUG
%     % disp(['k: ', num2str(k), ' | v: ', num2str(v_hist(k+1)), ' | i_ref(k+1) = ', num2str(i_ref(k+1)), ' | u: ', num2str(u_hist(k+1))])
%     % psit
% end
