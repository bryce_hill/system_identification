% closed loop ID with noise
%

% fft parameters
delt = .01; % time sampling period
N = 256; % fft record length [samples]
m = 20; % total number of N-periods simulated
l = 2; % N-period where fft sample is taken.
l2 = 20; % N-period where fft sample is taken to establish accuracy standard
t = [1:m*N]*delt; % simulation time record
T = t(N); % fft record length [sec]
delw = 2*pi/T; % fft frequency sampling resolution [rad/sec]
w = [0:N/2-1]*delw; % fft frequency vector (up to nyquist freq.)
f = w/(2*pi); % fft frequency in Hz

% plant description
wn = 2*pi*3;
zeta = 0.9;
num=[0 0 wn^2];
den=[1 2*zeta*wn wn^2];
[mag,pha]=bode(num,den,w);
H = mag'.*exp(j*pha'*pi/180);
tsol = 3/(min(-real(roots(den))));

% controller
k = 10;
Qnum = [0 k];
Qden = [1 0];
[Qmag,Qpha]=bode(Qnum,Qden,w);
olnum = conv(num,Qnum);
olden = conv(den,Qden);
clnum = olnum;
clden = olden + olnum;
tscl = 3/(min(-real(roots(clden))));

figure(1)
% clg
[olmag,olpha]=bode(olnum,olden,w(2:length(w)));
subplot(211)
semilogx(f(2:length(f)),20*log10(olmag))
hold on
semilogx(f(2:length(f)),zeros(1,length(f)-1),'--')
ylabel('loop gain magnitude, [dB]')
subplot(212)
semilogx(f(2:length(f)),olpha)
hold on
semilogx(f(2:length(f)),-180*ones(1,length(f)-1),'--')
ylabel('- loop gain angle, [deg]')
xlabel('frequency, [Hz]')

% plant input simulation model in closed loop
unum = conv(Qnum,den);
uden = clden;
[umag,upha]=bode(unum,uden,w);
[clmag,clpha]=bode(clnum,clden,w);



% random input
ur0 = randn(size(t)) + .06;
%[B,A]=butter(10,0.95);
%filter(B,A,ur0);
ur = lsim(unum,uden,ur0,t)' + 0.001*randn(size(t));
Ur = 2/N*fft(ur((l-1)*N+1:l*N));
Ur(1) = Ur(1)/2;
urrms = sqrt(dot(ur,ur)/(m*N));
yr = lsim(clnum,clden,ur0,t)' + 0.001*rand(size(t));
Yr = 2/N*fft(yr((l-1)*N+1:l*N));
Yr(1) = Yr(1)/2;

% pseudo random input and synthetic output
up0 = zeros(size(t))+.06;
yps = zeros(size(t))+.06*mag(1);
norm = 16; % to obtain similar rms values for ur and up
for i=1:N/2-1
	phi = rand*2*pi;
	up0 = up0 + 1/norm*cos(2*pi*i/T*t + phi);
	yps = yps + 1/norm*clmag(i+1)*cos(2*pi*i/T*t + phi + clpha(i+1)*pi/180);
end
up = lsim(unum,uden,up0,t)' + 0.001*randn(size(t));
Up = 2/N*fft(up((l-1)*N+1:l*N));
Up(1) = Up(1)/2;
uprms = sqrt(dot(up,up)/(m*N));
yp = lsim(clnum,clden,up0,t)' + 0.001*randn(size(t));
Yps = 2/N*fft(yps((l-1)*N+1:l*N));
Yps(1) = Yps(1)/2;
Yp = 2/N*fft(yp((l-1)*N+1:l*N));
Yp(1) = Yp(1)/2;
Yp2 = 2/N*fft(yp((l2-1)*N+1:l2*N));
Yp2(1) = Yp2(1)/2;

figure(2)
% clg
subplot(211)
plot(t((l-1)*N+1:l*N),ur((l-1)*N+1:l*N))
xlabel('time, [sec]')
title('Random Excitation Plant Input')

ylabel('amplitude of ur')
subplot(212)
plot(f,abs(Ur(1:N/2)))
xlabel('frequency, [Hz]')
ylabel('ur fft magnitude')

figure(3)
% clg
subplot(211)
plot(t((l-1)*N+1:l*N),yr((l-1)*N+1:l*N))
hold on
%plot([ts,ts],[-10 10])
%axis([t(1) t(N) -0.5 0.5])
xlabel('time, [sec]')
ylabel('amplitude of yr')
title('Plant Output Using Random Excitation')
subplot(212)
plot(f,abs(Yr(1:N/2)))
xlabel('frequency, [Hz]')
ylabel('yr fft magnitude')

figure(4)
% clg
subplot(211)
plot(t((l-1)*N+1:l*N),up((l-1)*N+1:l*N))
xlabel('time, [sec]')
ylabel('amplitude of up')
title('Pseudo-Random Excitation Plant Intput')
subplot(212)
plot(f,abs(Up(1:N/2)))
xlabel('frequency, [Hz]')
ylabel('up fft magnitude')

figure(5)
% clg
subplot(211)
plot(t((l-1)*N+1:l*N),yp((l-1)*N+1:l*N))
hold on
%plot([ts,ts],[-10 10])
%axis([t(1) t(N) -0.5 0.5])
xlabel('time, [sec]')
ylabel('amplitude of yp')
title('Plant Output Using Pseudo-Random Excitation')
subplot(212)
plot(f,abs(Yp(1:N/2)))
xlabel('frequency, [Hz]')
ylabel('yp fft magnitude')

% ETFE using up
Hp = Yp./Up;
Hp2 = Yp2./Up; % accuracy standard
figure(6)
% clg
subplot(211)
semilogx(f,20*log10(abs(Hp(1:N/2))))
hold on
semilogx(f,20*log10(mag),'c')
axis([f(1) f(N/2) -50 10])
ylabel('magnitude of Hp, [dB]')
title('ETFE using pseudo-random input')

subplot(212)
semilogx(f,180/pi*unwrap(angle(Hp(1:N/2))))
hold on
semilogx(f,pha,'c')
axis([f(1) f(N/2) -200 20])
ylabel('angle of Hr, [deg]')
xlabel('frequency, [Hz]')




% ETFE using ur
Hr = Yr./Ur;
figure(7)
% clg
subplot(211)
semilogx(f,20*log10(abs(Hr(1:N/2))))
hold on
semilogx(f,20*log10(mag),'c')
axis([f(1) f(N/2) -50 10])
ylabel('magnitude of Hr, [dB]')
title('ETFE using random input')

subplot(212)
semilogx(f,180/pi*unwrap(angle(Hr(1:N/2))))
hold on
semilogx(f,pha,'c')
axis([f(1) f(N/2) -200 20])
ylabel('angle of Hr, [deg]')
xlabel('frequency, [Hz]')



figure(8)
% clg
subplot(211)
semilogx(f,100*abs(Hr(1:N/2)-Hp2(1:N/2))./abs(Hp2(1:N/2)))
ylabel('Hr error magnitude, [percent]')
axis([f(1) f(N/2) 0 100])
title('Error in ETFE Using Random Input')
subplot(212)
semilogx(f,100*abs(Hp(1:N/2)-Hp2(1:N/2))./abs(Hp2(1:N/2)))
xlabel('frequency, [Hz]')
ylabel('Hp error magnitude, [percent]')
axis([f(1) f(N/2) 0 100])
title('Error in ETFE Using Pseudo-Random Input')


% averaged spectral density estimate using ur and ETFE estimate using up

% average over later records
nHps = zeros(1,N);
dHps = zeros(1,N);
Hpa = zeros(1,N);

for m=l:l2


Up = 2/N*fft(up((m-1)*N+1:m*N));
Up(1) = Up(1)/2;
Yp = 2/N*fft(yp((m-1)*N+1:m*N));
Yp(1) = Yp(1)/2;

Ur = 2/N*fft(ur((m-1)*N+1:m*N));
Ur(1) = Ur(1)/2;
Yr = 2/N*fft(yr((m-1)*N+1:m*N));
Yr(1) = Yr(1)/2;

% average numerator and denominator separately for spectral est.
nHps = nHps + (Yr.*conj(Ur))/(l2-l+1);
dHps = dHps + (Ur.*conj(Ur))/(l2-l+1);

% average ETFE result for comparison
Hpa = Hpa + (Yp./Up)/(l2-l+1);

end

Hps = nHps./dHps;

figure(9)
% clg
subplot(211)
semilogx(f,20*log10(abs(Hps(1:N/2))),'r')
hold on
semilogx(f,20*log10(abs(Hpa(1:N/2))),'b--')
semilogx(f,20*log10(mag),'c')
axis([f(1) f(N/2) -50 10])
ylabel('magnitude of Hps, [dB]')
title('Correlation Estimate Using Random Excitation')
text(1,-30,'Number of Averages = 18')

subplot(212)
semilogx(f,180/pi*unwrap(angle(Hps(1:N/2))),'r')
hold on
semilogx(f,180/pi*unwrap(angle(Hpa(1:N/2))),'b--')
semilogx(f,pha,'c')
axis([f(1) f(N/2) -200 20])
ylabel('angle of Hr, [deg]')
xlabel('frequency, [Hz]')
legend('Correlation','Averaged Pseudo Random ETFE')
