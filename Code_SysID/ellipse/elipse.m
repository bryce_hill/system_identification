% m-file to compute elipsoid bounds in parameter space
%
% time vector
Ts = 1;
t=[1:1000]'*Ts; % [sec]
% input signal
w0=0.9; % [rad/sec];
%u = sin(w0*t);
u= 10*rand(size(t));
% plant
num=[0 .4];
den=[1 .35];
[numd,dend]=c2dm(num,den,Ts,'zoh');
% plant output
ybar=filter(numd,dend,u);
% disturbance
amp = 0.1;
nu = 2*amp*(rand(size(t)) - 0.5);
N = 100; % number of points in the data vectors
n = 500; % starting point
w = filter(dend,[1 0],nu)
wsqest = 1.0*w(n+1:n+N)'*w(n+1:n+N);
y = ybar + nu;
% data vectors
Y = y(n+1:n+N);
Phi = [y(n:n+N-1) u(n:n+N-1)]';
R = Phi*Phi';
P = -2*Phi*Y;
C = Y'*Y - wsqest;

figure(1)
plot(t(n:n+N-1),u(n:n+N-1))
xlabel('time, [sec]')
ylabel('input u')

figure(2)
plot(t(n:n+N-1),y(n:n+N-1))
xlabel('time, [sec]')
ylabel('output y')

nprp = 0.25*P'*inv(R)*P - C;
thetazero = -0.5*inv(R)*P;
gamma = [1:501]*2*pi/500;
for i=1:501
	rot = [cos(gamma(i)) sin(gamma(i))]';
	rgamma(i) = sqrt(nprp/(rot'*R*rot));
	elip(i,:) = thetazero' + rgamma(i)*rot';
end

figure(3)

plot(elip(:,1),elip(:,2))
hold on
plot(thetazero(1),thetazero(2),'+')
plot(-dend(2),numd(2),'o')
xlabel('parameter estimate alpha')
ylabel('parameter estimate beta')


% frequency response bounds from elipsoid bounds

for l = [1:100];
omeg(l) = l*pi/Ts/100;
for i=1:501
	rg = [0:20]*rgamma(i)/20;
	al = thetazero(1) + rg*cos(gamma(i));
	be = thetazero(2) + rg*sin(gamma(i));
	a = 1/Ts*log(1./al);
	b = be.*a./(1-al);
	h = b./(j*omeg(l) + a);
	
	magmin(i) = min(abs(h));
	magmax(i) = max(abs(h));
	phamin(i) = min(angle(h));
	phamax(i) = max(angle(h));

end
mmin(l)=min(magmin);
mmax(l)=max(magmax);
pmin(l)=min(phamin);
pmax(l)=max(phamax);
magzero(l) = abs(h(1));
phasezero(l) = angle(h(1));
end


figure(4)

subplot(211)
semilogx(omeg/(2*pi),20*log10(magzero),'b')
hold on
semilogx(omeg/(2*pi),20*log10(mmax),'--')
semilogx(omeg/(2*pi),20*log10(mmin),'--')
xlabel('frequency, [Hz]')
ylabel('plant magnitude, [dB]')

subplot(212)
semilogx(omeg/(2*pi),180/pi*phasezero,'b')
hold on
semilogx(omeg/(2*pi),180/pi*pmax,'--')
semilogx(omeg/(2*pi),180/pi*pmin,'--')
xlabel('frequency, [Hz]')
ylabel('plant phase, [deg]')
