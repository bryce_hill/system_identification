% program to examine ETFE accuracy
%
% Dale Lawrence, 2/07/2008
%
% fft parameters
delt = .01; % time sampling period
N = 256; % fft record length [samples]
m = 4; % total number of N-periods simulated
l = 1; % N-period where fft sample is taken.
l2 = 4; % N-period where fft sample is taken to establish accuracy standard
t = [1:m*N]*delt; % simulation time record
T = t(N); % fft record length [sec]
delw = 2*pi/T; % fft frequency sampling resolution [rad/sec]
w = [0:N/2-1]*delw; % fft frequency vector (up to nyquist freq.)
f = w/(2*pi); % fft frequency in Hz

% plant description
wn = 2*pi*3;
zeta = 0.5;
pzsep = 100000; % pole-zero separation factor
num=[1 2*zeta*pzsep*wn (pzsep*wn)^2];
den=pzsep^2*[1 2*zeta*wn wn^2];
[mag,pha]=bode(num,den,w);
H = mag'.*exp(j*pha'*pi/180);
ts = 3/(min(-real(roots(den)))); % 5% settling time

% random input
ur0 = randn(size(t)) + .06;
[B,A]=butter(10,0.95);
ur = filter(B,A,ur0);
Ur = 2/N*fft(ur((l-1)*N+1:l*N));
Ur(1) = Ur(1)/2;
urrms = sqrt(dot(ur,ur)/(m*N));
yr = lsim(num,den,ur,t)';
Yr = 2/N*fft(yr((l-1)*N+1:l*N));
Yr(1) = Yr(1)/2;

% pseudo random input and synthetic output (using frequency resp. to
% calculate output sinusoidal components to avoid transients in the data)
up = zeros(size(t))+.06;
yps = zeros(size(t))+.06*mag(1);
norm = 16; % to obtain similar rms values for ur and up
for i=1:N/2-1
	phi = rand*2*pi;
	up = up + 1/norm*cos(2*pi*i/T*t + phi);
	yps = yps + 1/norm*mag(i+1)*cos(2*pi*i/T*t + phi + pha(i+1)*pi/180); % synthetic output
end
Up = 2/N*fft(up((l-1)*N+1:l*N)); % input spectrum
Up(1) = Up(1)/2; % correct for wrong DC gain normalization
uprms = sqrt(dot(up,up)/(m*N));
% simulate to get actual plant output
yp = lsim(num,den,up,t)';
Yps = 2/N*fft(yps((l-1)*N+1:l*N)); % spectrum of synthetic output (no transients)
Yps(1) = Yps(1)/2;
Yp = 2/N*fft(yp((l-1)*N+1:l*N));
Yp(1) = Yp(1)/2;
Yp2 = 2/N*fft(yp((l2-1)*N+1:l2*N));
Yp2(1) = Yp2(1)/2;

% plot random excitation source in time and frequency
figure(1)
clf
subplot(211)
plot(t((l-1)*N+1:l*N),ur((l-1)*N+1:l*N),'k')
xlabel('time, [sec]')
title('Random Excitation Source')
ylabel('amplitude of ur')
subplot(212)
plot(f,abs(Ur(1:N/2)),'k')
xlabel('frequency, [Hz]')
ylabel('ur fft magnitude')

% plot plant output from random input
figure(2)
clf
subplot(211)
plot(t((l-1)*N+1:l*N),yr((l-1)*N+1:l*N),'k')
hold on
%plot([ts,ts],[-10 10])
%axis([t(1) t(N) -0.5 0.5])
xlabel('time, [sec]')
ylabel('amplitude of yr')
title('Plant Output Using Random Excitation')
subplot(212)
plot(f,20*log10(abs(Yr(1:N/2))),'k')
xlabel('frequency, [Hz]')
ylabel('yr fft magnitude [dB]')

% plot pseudo-random input
figure(3)
clf
subplot(211)
plot(t((l-1)*N+1:l*N),up((l-1)*N+1:l*N),'k')
xlabel('time, [sec]')
ylabel('amplitude of up')
title('Pseudo-Random Excitation Source')
subplot(212)
plot(f,20*log10(abs(Up(1:N/2))),'k')
xlabel('frequency, [Hz]')
ylabel('up fft magnitude')

% plot plant output from pseudo random input
figure(4)
clf
subplot(211)
plot(t((l-1)*N+1:l*N),yp((l-1)*N+1:l*N),'k')
hold on
%plot([ts,ts],[-10 10])
%axis([t(1) t(N) -0.5 0.5])
xlabel('time, [sec]')
ylabel('amplitude of yp')
title('Plant Output Using Pseudo-Random Excitation')
subplot(212)
plot(f,20*log10(abs(Yp(1:N/2))),'k')
xlabel('frequency, [Hz]')
ylabel('yp fft magnitude')

% ETFE using pseudo random input
Hp = Yp./Up;
Hp2 = Yp2./Up; % accuracy standard
figure(5)
clf
subplot(211)
semilogx(f,20*log10(abs(Hp(1:N/2))),'k')
hold on
semilogx(f,20*log10(mag),'k--')
axis([f(1) f(N/2) -50 10])
ylabel('magnitude of Hp, [dB]')
title('ETFE using pseudo-random input')
subplot(212)
semilogx(f,180/pi*unwrap(angle(Hp(1:N/2))),'k')
hold on
semilogx(f,pha,'k--')
axis([f(1) f(N/2) -200 20])
ylabel('angle of Hr, [deg]')
xlabel('frequency, [Hz]')




% ETFE using random input
Hr = Yr./Ur;
figure(6)
clf
subplot(211)
semilogx(f,20*log10(abs(Hr(1:N/2))),'k')
hold on
semilogx(f,20*log10(mag),'k--')
axis([f(1) f(N/2) -50 10])
ylabel('magnitude of Hr, [dB]')
title('ETFE using random input')

subplot(212)
semilogx(f,180/pi*unwrap(angle(Hr(1:N/2))),'k')
hold on
semilogx(f,pha,'k--')
axis([f(1) f(N/2) -200 20])
ylabel('angle of Hr, [deg]')
xlabel('frequency, [Hz]')


% compare ETFE errors 
figure(7)
clf
subplot(211)
semilogx(f,100*abs(Hr(1:N/2)-Hp2(1:N/2))./abs(Hp2(1:N/2)),'k')
ylabel('Hr error magnitude, [percent]')
axis([f(1) f(N/2) 0 100])
title('Error in ETFE Using Random Input')
subplot(212)
semilogx(f,100*abs(Hp(1:N/2)-Hp2(1:N/2))./abs(Hp2(1:N/2)),'k')
xlabel('frequency, [Hz]')
ylabel('Hp error magnitude, [percent]')
axis([f(1) f(N/2) 0 100])
title('Error in ETFE Using Pseudo-Random Input')