% file to compute accuracy of FFT
%
w = 2*pi; % frequency bin of interest [rad/sec]
delt = .01; % time sampling period
phi = 1; % phase shift of sinusoid [rad]
w0 = [w:.01*2*pi:w+2*pi]; % 1 Hz variation in sine wave freq.
em = exp(j*(w0-w)*delt);
ep = exp(-j*(w0+w)*delt);

figure(1)
clf

for i = [1:5];
	N = i/delt;
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	subplot(211)
	plot((w0-w)/(2*pi),abs(xf(i,:)))
	hold on
	subplot(212)
	plot((w0-w)/(2*pi),unwrap(angle(xf(i,:))))
	hold on
end
subplot(212)
plot((w0-w)/(2*pi),ones(1,length(xf(i,:)))*phi,'--')
text(0.1,20,'Signal Phase = 1 rad')
subplot(211)
xlabel('frequency error, [Hz]')
ylabel('magnitude of fft coefficient')
title('FFT coefficent at center bin versus signal freq. error')
text(0.4,0.85,'Record Lengths of 1,2,3,4,5 sec')
subplot(212)
xlabel('frequency error, [Hz]')
ylabel('angle of fft coefficient [rad]')


figure(2)
clf
t = [0:delt/10:1];
for l=1:10:101
	xt = cos(w0(l)*t + phi);
	plot(t,xt)
	hold on
end
xlabel('time, [sec]')
ylabel('time signal')
title('Time signal for frequencies w0=1.0+0.1*i Hz')

figure(3)
clf
plot(w0/(2*pi),abs(xf(i,:)))

figure(4)
clf
for i = [1:5];
	N = i/delt;
	w0 = [w:5*.01*2*pi/N/delt:w+5*2*pi/N/delt]; % 5 bin variation 
	em = exp(j*(w0-w)*delt);
	ep = exp(-j*(w0+w)*delt);
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	subplot(211)
	plot((w0-w)/(2*pi/N/delt),abs(xf(i,:)))
	hold on
	subplot(212)
	plot((w0-w)/(2*pi/N/delt),unwrap(angle(xf(i,:))))
	hold on
end
subplot(212)
plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:)))*phi,'--')
text(0.1,20,'Signal Phase = 1 rad')
subplot(211)
xlabel('frequency error, [bins]')
ylabel('magnitude of fft coefficient')
title('FFT coefficent at center bin versus signal freq. error')
text(1,0.8,'Record Lengths of 1,2,3,4,5 sec')
subplot(212)
xlabel('frequency error, [bins]')
ylabel('angle of fft coefficient')



figure(5)
clf
t = [0:delt:10-delt];
Nn = 10/(delt);
w = [0:Nn/2-1]*pi/delt/(Nn/2);
for l=1:6
	xt = cos(w0(l)*t + phi);
	Xw=2/Nn*fft(xt);
	subplot(211)
	plot(w,abs(Xw(1:Nn/2)))
	hold on
	subplot(212)
	plot(w,(angle(Xw(1:Nn/2))))
	hold on
end
subplot(211)
plot([6.2832 6.2832],[0 2],'--')
plot([6.9115 6.9115],[0 2],'--')
plot([7.5398 7.5398],[0 2],'--')
axis([0 4*pi 0 1.2])
xlabel('frequency, [rad/sec]')
ylabel('magnitude of fft coefficient')
title('FFT result for various signal frequencies w0')
text(1,0.9,'w0 = 6.2832')
text(1,0.8,'w0 = 6.3460')
text(1,0.7,'w0 = 6.4000')
text(1,0.6,'w0 = 6.4717')
text(1,0.5,'w0 = 6.5345')
text(1,0.4,'w0 = 6.5973')

text(8,0.9,'freq bin = 6.2832')
text(8,0.8,'freq bin = 6.9115')
text(8,0.7,'freq bin = 7.5398')

text(1,1.09,'Record length = 10 sec')
text(1,1,'Signal phase = 1 rad')

subplot(212)
plot(w,phi*ones(length(w)),'--')
plot([6.2832 6.2832],[-7 7],'--')
plot([6.9115 6.9115],[-7 7],'--')
plot([7.5398 7.5398],[-7 7],'--')
axis([0 4*pi -4 4])
xlabel('frequency, [rad/sec]')
ylabel('angle of fft coefficient [rad]')


w = 2*pi;
w0 = [w:.01*2*pi:w+2*pi]; % 1 Hz variation in sine wave freq.
em = exp(j*(w0-w)*delt);
ep = exp(-j*(w0+w)*delt);

figure(6)
clf
delphi = pi/12;
for i = [1:20];
	N = 1/delt;
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi*(i-1))*(1 - em.^N)./(1-em) +...
		   exp(-j*phi*(i-1))*(1-ep.^N)./(1-ep))/N;
	subplot(211)
	plot((w0-w)/(2*pi),abs(xf(i,:)))
	hold on
	subplot(212)
	plot((w0-w)/(2*pi),unwrap(angle(xf(i,:)))-phi*(i-1))
	hold on
end
subplot(211)
plot((w0-w)/(2*pi),ones(length(w0)),'-.')
text(0.1,0.2,'Signal Phase = (pi/12)*i')
text(.6,1.2,'Record Length = 1 sec')
xlabel('frequency error, [Hz]')
ylabel('magnitude of fft coefficient')
title('FFT coefficent at center bin versus signal freq. error')
subplot(212)
xlabel('frequency error, [Hz]')
ylabel('angle error of fft coefficient')
