% file to compute accuracy of FFT
%
w = 2*pi; % frequency bin of interest [rad/sec]
delt = .01; % time sampling period
phi = 0; % phase shift of sinusoid (input)
theta = pi/2; % phase shift of sinusoid (output)
w0a = [w+.01*2*pi:.01*2*pi:w+2*pi]; % 1 Hz variation in sine wave freq.
em = exp(j*(w0a-w)*delt);
ep = exp(-j*(w0a+w)*delt);

figure(1)
% clg

for i = [1:5];
	N = i/delt;
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	yf(i,:) = (exp(j*theta)*(1 - em.^N)./(1-em) +...
		   exp(-j*theta)*(1-ep.^N)./(1-ep))/N;
	subplot(211)
	plot((w0a-w)/(2*pi),abs(yf(i,:)./xf(i,:)))
	hold on
	subplot(212)
	plot((w0a-w)/(2*pi),angle(yf(i,:)./xf(i,:)))
	hold on
end
subplot(212)
plot((w0a-w)/(2*pi),ones(1,length(xf(i,:)))*(theta-phi),'--')
subplot(211)
xlabel('frequency error, [Hz]')
ylabel('amplitude of fft coefficient')
subplot(212)
xlabel('frequency error, [Hz]')
ylabel('angle of fft coefficient')


figure(2)
% clg
T = 5; % record length in sec
t = [0:delt:T-delt/10];
N = 5/delt; % record length in points
ww = [0:2*pi/T:2*pi/delt/2-2*pi/T];
for l=10 % frequency error index (corresponds to 1/2 bin)
	xt = cos(w0a(l)*t + phi);
	Xw = 2/(5/delt)*fft(xt);
	yt = cos(w0a(l)*t + theta);
	Yw = 2/(5/delt)*fft(yt);
	subplot(211)
	magerr = abs(1-abs(Yw(1:N/2)./Xw(1:N/2)))*100;
	plot(ww,magerr)
	%plot(ww,abs(Xw(1:N/2)))
	axis([0 20 0 100])
	subplot(212)
	phaerr = (theta-phi-angle(Yw(1:N/2)./Xw(1:N/2)))*180/pi;
	plot(ww,phaerr)
	%plot(ww,abs(Yw(1:N/2)))
	axis([0 20 -10 10])
	%axis([0 2 theta-phi-1 theta-phi+1])
end
subplot(211)
xlabel('frequency, [rad/sec]')
ylabel('ETFE amplitude error, [percent]')
title('ETFE computed using fft')
text(8,80,'center bin at 6.2832 rad/sec')
subplot(212)
xlabel('frequency, [rad/sec]')
ylabel('ETFE phase error, [deg]')

figure(3)
% clg

I = [1:1];
for i = I;
	N = i/delt;
	% 0.5 bin variation in signal frequency
	w0 = [w+0.5*.01*2*pi/N/delt:0.5*.01*2*pi/N/delt:w+0.5*2*pi/N/delt];
 	em = exp(j*(w0-w)*delt);
	ep = exp(-j*(w0+w)*delt);
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	yf(i,:) = (exp(j*theta)*(1 - em.^N)./(1-em) +...
		   exp(-j*theta)*(1-ep.^N)./(1-ep))/N;
	magx = abs(xf(i,:));
	phax = angle(xf(i,:));
	magy = abs(yf(i,:));
	phay = angle(yf(i,:));

	subplot(211)
 	plot((w0-w)/(2*pi/N/delt),magx)
	hold on
	plot((w0-w)/(2*pi/N/delt),magy,'--')

	subplot(212)
	plot((w0-w)/(2*pi/N/delt),(phi-phax)*180/pi)
	hold on
	plot((w0-w)/(2*pi/N/delt),(theta-phay)*180/pi,'--')

end

subplot(211)
plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:))),'-.')
xlabel('frequency error, [bins]')
ylabel('magnitude of FFT')
title('FFT error due to frequency error, N = 1 period')
%axis([0 0.5 0.7 1.3])
legend('Input','Output')
subplot(212)
%plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:)))*(phi)*180/pi,'-.')
%plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:)))*(theta)*180/pi,'-.')
xlabel('frequency error, [bins]')
ylabel('angle error of FFT, [deg]')
%axis([0 0.5 (theta+phi)/2*180/pi-90 (theta+phi)/2*180/pi+90])
text(.05,-40,'phi = 0')
text(.05,-60,'theta = 90')
axis([0 0.5 -100 10])




figure(4)
% clg

I = [1:1];
for i = I;
	N = i/delt;
	% 0.5 bin variation in signal frequency
	w0 = [w+0.5*.01*2*pi/N/delt:0.5*.01*2*pi/N/delt:w+0.5*2*pi/N/delt];
 	em = exp(j*(w0-w)*delt);
	ep = exp(-j*(w0+w)*delt);
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	yf(i,:) = (exp(j*theta)*(1 - em.^N)./(1-em) +...
		   exp(-j*theta)*(1-ep.^N)./(1-ep))/N;
	mag = abs(yf(i,:)./xf(i,:));
	pha = angle(yf(i,:)./xf(i,:));
	subplot(211)
	plot((w0-w)/(2*pi/N/delt),mag)
	hold on
	subplot(212)
	plot((w0-w)/(2*pi/N/delt),pha*180/pi)
	hold on
end

subplot(211)
plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:))),'--')
xlabel('frequency error, [bins]')
ylabel('magnitude of ETFE')
title('ETFE error due to frequency error, N = 1 period')
axis([0 0.5 0.7 2.3])
subplot(212)
plot((w0-w)/(2*pi/N/delt),ones(1,length(xf(i,:)))*(theta-phi)*180/pi,'--')
xlabel('frequency error, [bins]')
ylabel('angle of ETFE, [deg]')
axis([0 0.5 (theta-phi)*180/pi-20 (theta-phi)*180/pi+20])




figure(5)
% clg

for l = [0:5]
theta = l/5*pi/2;

I = [1:5];
for i = I;
	N = i/delt;
	% 0.5 bin variation in signal frequency
	w0 = [w+0.5*.01*2*pi/N/delt:0.5*.01*2*pi/N/delt:w+0.5*2*pi/N/delt];
 	em = exp(j*(w0-w)*delt);
	ep = exp(-j*(w0+w)*delt);
	% fft result in frequency bin of interest
	xf(i,:) = (exp(j*phi)*(1 - em.^N)./(1-em) +...
		   exp(-j*phi)*(1-ep.^N)./(1-ep))/N;
	yf(i,:) = (exp(j*theta)*(1 - em.^N)./(1-em) +...
		   exp(-j*theta)*(1-ep.^N)./(1-ep))/N;
	mag = abs(yf(i,:)./xf(i,:));
	mm(i) = max(abs(1-mag))*100; % in percent
	pha = angle(yf(i,:)./xf(i,:));
	mp(i) = max(abs(theta-phi-pha))*180/(pi); % in degrees
end


   
subplot(211)
plot(I,mm(I))
xlabel('length of time record, [periods]')
ylabel('percent error in magnitude')
title('Max ETFE error due to frequency error at Center Bin')
text(2,40,'relative phases 0,18,36,54,72,90 deg')
hold on
subplot(212)
plot(I,mp(I))
xlabel('length of time record, [periods]')
ylabel('phase error, [deg]')
hold on

end





