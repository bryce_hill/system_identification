function [sys] = servomodel()
% Class:      ASEN 6519 - System Identification
% 
% File:       servomodel.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Create servo model

s = tf('s');
Kt = .1;
Ka = .5;
Kf = 636;
Kdac = .00488;
J = 10*10^-4;
P = .01;
D = .1;

% L = (Ka*Kt)/(J*s^2)*Kf*(P+s*D)*Kdac
% figure
% step(L)
% figure
% step(feedback(L,1))

num = Ka*Kt*Kf*Kdac.*[D P];
den = J.*[1 0 0];
T = tf(num, den)

[As, Bs, Cs, Ds] = tf2ss(num, den);
sys = ss(As, Bs, Cs, Ds);

dt = .01;
tlen = .5;
t = 0:dt:tlen;
r = ones(1, numel(t));
% figure
% [y, ~, x] = lsim(sys, r, t)
sys2 = feedback(sys, 1);
figure 
[y2, ~, x] = lsim(sys2, r, t);
plot(t, r,'--k')
hold on, grid on
plot(t, y2)
title('Step Response of Servo Model'), xlabel('Time [s]')

end



