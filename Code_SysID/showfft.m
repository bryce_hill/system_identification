function [f, fft_d] = showfft(data, descript, pparms)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%   INPUTS:
%   Data   = Values to be fft'd
%   pparms: plot parameters struct
%           pparms.splot = true/false to plot or not
%           pparms.sub_title = subtitle plot titles
%           pparms.mag = 'mag' or 'db'
% 
%   OUTPUTS:

N = pparms.nfreq;
T = pparms.T; 

wp = (2*pi/T*(0:1:(N/2)-1));        % Define fft frequencies
f = [-flip(wp) wp(2:end)];        % (2:end) is to get rid of duplicate 0

fft_d = fftshift(2/N*fft(data, N)); % Compute fft and shift it to be double/sided
fft_d = fft_d(2:end);               % Get rid of extra zero at the beginning
                                    % of the vector which doesn't match up 
                                    % with the frequencies

if pparms.splot == true
    figure
    plot(f, abs(fft_d)), grid on
    title([descript, ''])
    ylabel([pparms.mag, ''])
    xlabel('Frequency [rad/s]')
end
end

