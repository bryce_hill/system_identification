function simplot3(r, y_hist, t_hist, title_p)
% Class:      ASEN 6519 - System Identification
% 
% File:       performance.m
% 
% Created:    February 2017
% Authors:    Bryce Hill
%             
% Purpose:    Plot only r and y
    
% Plot the output of the system vs. reference command
figure
% subplot(2,1,1)

plot(t_hist, y_hist(:,1));
plot(t_hist, r, '--');
grid on
title(['Tracking vs R (', title_p, ')'])
legend('Output', 'Ref. \deltaZ Input')

% Plot states vs R
% figure
% subplot(2,1,2)
% plot(t_hist, x_hist(:, 1:nStates)),  hold on, grid on
% title(['States vs Time (', title_p, ')'])
% legend('\deltau (v_x)','\deltaw (v_z)','\deltaq (pitch rate)','\delta\theta (pitch \angle)','\deltax (pose)','\deltaz (pose)','Location','northwest')
% xlabel('Time [s]')

end