% h = [nan, 1; 2, inf];
% h_nan = any(isnan(h(:)))
% h_inf = any(isnan(h(:)))
% if (h_nan == true) || (h_inf == true)
%     disp('nan present')
% end

%% FFt test
disp('---------------- fft --------------')

decades = 1                 % Choose # of decades above and below center frequency to calculate
divisor = 10^decades;
ubound = divisor/10
wc = 1                      % [rad/s] my systems "critical" frequency
dw = wc/divisor             % [rad/s] delta omega
T = ((2*pi)/dw)             % [s] period. Rounded so that N is divisible by 2
dt = 2*pi/(2*(ubound)*wc)    % [s] dt nyquist at # decade above wc
N = T/dt                    % [] 

% --  Pseudo Random Excitation --
nfunc = round(N/2-1)        % Number of cosine functions 
phases = 2*pi*rand(1,nfunc);% Random phases
% ts = 10;                  % Settling time
mx = 10;                    %
time = 0:dt:mx*T-dt;           % Total simulation time 
% time = time(1:end-1);
freqs = (0:1:nfunc)*dw;   % Generate range of frequencies
% freqs = (dw:1:nfunc+1)*dw;  % Generate range of frequencies
dc_flag = 0;

amp = ones(1,nfunc);        % Generate amplitude weight matrix
% for i = 1:1:nfunc
% %    amp(i) = 100000/(i^3); 
% %    amp(i) = (i)/100; 
%     amp(i) = 100/(i); 
% end
% iamp = find(amp<1);
% amp(iamp) = 1;

pr = zeros(1,numel(time));

for i = 1:1:nfunc
   pr = pr + amp(i)*cos(freqs(i)*time + phases(i));
end

% Nfft = length(pr)-1;
Nfft = N
% fvals = (-Nfft/2:Nfft/2-1)/Nfft;
fvals = [-flip(freqs) freqs];

pr_fft = 2/Nfft*fft(pr, Nfft);  % Calculate fft and normalize by 2/Nfft
pr_fft_D = fftshift(pr_fft);

figure
plot(fvals, abs(pr_fft_D)), grid on
title('Double Sided FFT of pseudo Random Excitation')
ylabel('DFT Values [mag]')
xlabel('Frequency [rad/s]')


pparms.splot = true;
pparms.mag = 'mag';
pparms.nfreq = N;
pparms.T = T;
[wpfunc, fft_data] = showfft(pr, 'Test fft', pparms);