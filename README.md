## Created January, 2017
## System Identification Project(ASEN 6519 Class Repository)
## Authors: Bryce Hill

### ------- Purpose  : -------

***Note: This repository is a work in progress!**

Use system Identification techniques to determine the plant of a longitudinal aircraft system (using a known plant as truth):

This repository was created as a class repository for ASEN 6519. However, it contains very useful matlab system analysis, control and identification code.

### TODO:
- Demonstrate various methods of control. | CHECK

- Perform Empirical Transfer Function Estimate (ETFE). | CHECK

- Perform in depth modal space analysis. | IN PROGRESS


### ------- Directory : -------

### Code_SysID:

Main system identification project

### Code_Servo_Augmented_Model:

Contains a servo augmented version of the plant. The 		servo model selected was weakly controllable and 	observable and when augmented with the plane model the 	whole system went uncontrollable/observable. This was an 	attempt to add dynamics to the system

### Code_Control_Demonstration (same code as Sys_ID with different options selected):

The code used for the system identification required plant modification from MIMO to a SIMO system that renders some of these demonstrated control approaches ineffective. So, this uses the original MIMO model to demonstrate the following control methods:

- Full State Feedback
- Integral
- Observer + Integral
- LQR
- LQR + Integral
- LQR + Integral + Observer